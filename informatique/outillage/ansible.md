---
title: Ansible
description: Ansible est un outils de permettant d'orchestrer des recettes (playbook) sur un ensemble d'hôtes.
published: true
date: 2021-03-17T20:34:49.934Z
tags: devops, ansible, iac, configuration
editor: markdown
dateCreated: 2020-11-21T22:25:05.540Z
---

> En cours
{.is-warning}

> [Les commandes ansible](/linux/commandes/ansible) que j'utilise
{.is-info}

## Inventaire

L'inventaire est en la liste des hôtes sur lesquels il est possible d'appliquer des playbooks. Ceux-ci peuvent être classé dans différents groupes. Hôtes et groupes peuvent contenir leur propres variables.

### Fichier d'inventaire

> à écrire
{.is-warning}

Le fichier inventaire peut-être remplacé par un plugin d'inventaire


### Variables

Les variables peuvent être définies selon deux niveaux:

* les variables de l'hôte (*host var*), elles sont stockées dans le sous-répertoire `host_vars`
* les variables de groupe (*group var*), elles sont stockées dans le sous-répertoire `group_vars`

Si l'on dispose que d'un seul fichier de variable celui-ci peut être `<hôte_ou_groupe>.yaml`. Mais si l'on a besoin de plusieurs fichiers variables, alors on peut les stocker dans un sous-répertoire `<hôte_ou_groupe>/<fichier>.yaml`.
Utiliser plusieurs fichiers variables pour un même groupe ou un même hôte s'avère très utile quand les variables sont nombreuses ou lors que l'on souhaite avoir un fichier *ansible vault* pour cet hôte ou groupe.
Quand on souhaite appliquer une variable à tous les hôtes, il est possible de créer un fichier variable pour le *all*

Exemple de structure inventaire:

```
├── inv
│   ├── group_vars
│   │   ├── all
│   │   │   ├── vars1.yaml
│   │   │   ├── vars2.yaml
│   │   │   └── vault.yaml
│   │   ├── group1.yaml
│   │   └── group2
│   │       └── vars.yaml
│   ├── host_vars
│   │   ├── host1
│   │   │   └── vars.yaml
│   │   └── host2.yaml
│   └── hosts.yaml

```

### Plugin inventaire

Pour facilement lister ses serveurs et aggréger ses variables dans Ansible, il n'est pas forcément nécessaire de créer manuellement son inventaire. On peut faire appel à un [plugin inventaire](https://docs.ansible.com/ansible/latest/collections/community/general/index.html).

Ex:

* [Scaleway](https://docs.ansible.com/ansible/latest/collections/community/general/scaleway_inventory.html#ansible-collections-community-general-scaleway-inventory)
* [Hetzner](https://docs.ansible.com/ansible/latest/collections/hetzner/hcloud/hcloud_inventory.html)

> Attention, ces plugins ont besion d'un TOKEN, ne pas le mettre en clair sur un dépot Git.
> Soit chiffrer la déclaration à l'aide d'`ansible-vault` ou utiliser des variables d'environnements.
{.is-warning}

Pour Hetzner, il est nécessaire mettre la déclaration dans un fichier `<nom_du_fichier>.hcloud.yaml`

Ici l'inventaire permet de créer des groupes sur le *label* `usage` qui dans mon cas à la valeur `db` ou `k3s` 

```yaml
plugin: hcloud
keyed_groups:                  
  - key: labels.usage          
    separator: ""
```

