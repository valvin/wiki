---
title: Logiciels Libres
description: Les différents logiciels libres / opensource que j'utilisent
published: true
date: 2022-11-20T19:17:59.598Z
tags: libre, opensource, logiciels, foss
editor: markdown
dateCreated: 2020-04-09T12:39:40.498Z
---

## Les services/outils que j'héberge

* [Wiki.js *wiki avec édition markdown utilisé ici*](wikijs)
* [Tiny tiny RSS *gestionnaire de flux RSS*](ttrss)
* [Wallabag *lecteur de contenu déconnecté*](wallabag)
* [Owncast](/logiciels-libres/owncast)
* Lichess-info
* Gitea
* ~~[Friendica *réseau social décentralisé*](friendica)~~
{.links-list}