---
title: Auto-hébergement
description: depuis plusieurs années, je tente d'héberger moi même différents services avec plus ou moins de succès
published: true
date: 2022-11-22T21:33:35.900Z
tags: auto-hébergement
editor: markdown
dateCreated: 2022-11-22T21:10:54.379Z
---

# Liste des services auto-hébergés

## Actifs

* [Tiny Tiny RSS](/informatique/logiciels-libres/ttrss) - depuis 2015
* Wallabag - depuis 2018
* [RSS bridge](/informatique/logiciels-libres/rssbridge) - depuis 2020
* [WikiJS](/informatique/logiciels-libres/wikijs) - depuis 2020
* [Owncast](/informatique/logiciels-libres/owncast) - depuis 2021
* [Lichess Info](/informatique/logiciels-libres/lichess-info) - depuis 2021
* [Gemini](/informatique/gemini) - Agate + kiln - depuis 2022
* Gitea - depuis 2022
* Tor Relay - depuis 2022

## Inactifs

* Friendica - 2019 - 2022
  * raison de l'arrêt: trop de soucis d'administration. principalement lié à l'installation containérisée qui posait souvent souci à la montée de version de BDD et au stockage partagé entre les différents containers pour lequel j'avais opté pour du NFS.
  * remplacement: retour à Mastodon (framapiaf.org)
* Messagerie - 2015 - 2018 :
  * raison de l'arrêt: hébergé sur mon synology, je me suis retrouvé coincé par des anti-spams qui considérait que l'IP de ma box n'était pas suffisament légitime.
  * remplacement: Tutanota puis Protonmail puis Gandi
* Ghost (blog) - 2015 - ?
  * raison de l'arrêt: je ne sais plus trop ;-) je pense que j'avais le souhait de m'orienter vers un stockage du contenu au format markdown plutôt qu'intégrer dans une interface web.
  * remplacement: hugo + framagit.org (Gitlab Pages)

# Matériel utilisé

> Ma mémoire me fait défaut et j'ai oublié certains détails historique :)
{.is-info}
* Synology DS214 - 2015 à aujourd'hui
* Serveur OVH - ?? - 2017?
* Serveur VPS scaleway - 2018 à aujourd'hui
  * VC1S : VM (encore actif)
  * autres serveurs dont le prix a subittement augmenté
* Serveurs Hetzner - 2020 - 2022
  * CPX11 : VM 2vcpu / 2GB RAM - maitre cluster + noeud k3s
  * CPX31 : VM 4vcpu / 8GB RAM - noeud cluster k3s
  * CPX21 : VM 2vcpu / 4GB RAM - serveur bdd postgres + mysql + NFS
* Serveur Kimsufi - 2022 à aujourd'hui
  * KS-17 : dédié 4c/8t / 32Go RAM
 