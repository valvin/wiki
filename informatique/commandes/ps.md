---
title: ps - lister les process
description: commande ps
published: true
date: 2020-05-28T20:29:32.222Z
tags: linux, commande, ps
---

# ps

La commande `ps` permet de lister les processus en cours d'éxécution sur la machine.

[Page man](https://www.man7.org/linux/man-pages/man1/ps.1.html)

## lister tous les processus

avec celle-ci on liste tout les processus et on retrouvera les informations comme le propriétaire, l'id du process et celui de son père, le temps CPU et bien entendu la commande.

```bash
ps -ef
```

avec plus d'information dont l'usage de la mémoire, du cpu en pourcentage, son statut et aussi la quantité de mémoire résidente (RSS) et virtuelle (VSZ).
La mémoire résidente correspond à la mémoire réellement utilisée. 

La mémoire virtuelle contient la demande d'allocation mémoire mais aussi la taille du binaire et des librairies. (à clarifier)

```bash
ps -aux
```

On peut également trier la liste avec l'option `--sort`, celle que j'utilise est `--sort -rss` pour trier les process consommant le plus de mémoire.

## pour obtenir la commande complète

Pour afficher les longues commandes, on peut utiliser l'astuce suivante: 

```bash
ps -ef|cat -
```









