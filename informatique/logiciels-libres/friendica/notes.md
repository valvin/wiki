---
title: Friendica - notes
description: Différentes notes à trier
published: true
date: 2021-03-21T16:36:05.410Z
tags: friendica, notes, astuces
editor: markdown
dateCreated: 2020-05-01T21:45:02.620Z
---

## Fédération ActivityPub

Friendica peut se fédérer avec tous les produits qui supportent Activity Pub. Cependant cette option doit être activé en ligne de commande.

Cette fonctionnalité nécessite un relai qui utilise ce [projet](https://git.pleroma.social/pleroma/relay) et dont avoir une liste sur [the-federation.info](https://the-federation.info/#projects)

Pour ma part, j'utilise: 

```bash
$ bin/console relay list
https://relay.libranet.de/actor
https://rrfarmbot.appspot.com/actor
```

```bash
# pour ajouter un relai
$ bin/console relay add <url_du_relai>/actor
```

## Fil de messages

Dans la partie administration, il y a un indicateur très important. Il s'agit du nombre de messages en attentes. Au plus le nombre de messages est importants est moins c'est bon signe.

Il y a deux indicateurs : les messages différés et les messages eux mêmes. Un message différé est un message dont le traitement n'a pas abouti (serveur distant injoignable par exemple). Les autres messages sont ceux en attente de traitement. 

Un message peut être une nouvelle publication d'un serveur fédéré ou bien une publication de notre instance ou tout autres évènements que les différentes instances s'échangent.

Si ceux-ci sont trop nombreux, cela signifie qu'il y a un problème de *workers*. Un worker traite un ou plusieurs messages en fonction du paramétrage.

Il est important de vérifier le nombre de messages afin qu'il n'augmente pas. Il ne doit pas dépasser les 500.

```sql
# Defered message

select count(id) from workerqueue where done=0 and retrial > 0;

# Waiting messages:

select count(id) from workerqueue where done = 0 and retrial = 0;
```

## *Lock driver*

Lors d'une installation docker, il est important d'ajouter un *Locker driver*, il s'agit d'une fil de messages externalisées permettant de communiquer entre les différents containers. Cela permet notamment de vérouiller les traitements lors d'une montée de version.

Il s'agit concrètement d'une base de donnée *Redis*.

