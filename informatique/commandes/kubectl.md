---
title: Kubectl - commandes
description: commandes utiles avec kubectl / kubernetes
published: true
date: 2022-02-25T07:41:40.792Z
tags: kubectl, kubernetes
editor: markdown
dateCreated: 2022-02-25T07:41:40.792Z
---

# Les commandes pour `kubectl`


`kubectl` est indispensable pour administrer un cluster kubernetes ou même ne serait-ce que l'utiliser.

## Configuration

```bash
# pour savoir sur quel cluster on travaille et ceux présents dans la configuration
kubectl config get-contexts

# pour sélectionner un cluster
kubectl config use-context <nom_du_cluster>

```

## Obtention d'objet kubernetes

Les objets kubernetes sont définis au travers de *Custom Resources Definition* et sont définis en temps que *Kind*. Par exemple les plus connus sont `Pods`, `Deployment`, `Ingress`

```
# pour voir la liste d'un objet de type <kind> dans le namespace par défaut
kubectl get <kind>
# la même dans un namespace <namespace>
kubectl get <kind> -n <namespace>
# obtenir le manifest d'un objet au format yaml
kubectl get <kind> -o yaml
```