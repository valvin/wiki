---
title: LVM - manipulation de pv vg lv
description: ce qu'il faut savoir sur lvm pour jouer avec les physical volume, virtual group et logical volume
published: true
date: 2020-07-22T15:16:58.889Z
tags: linux, disque, lvm, lv, pv, vg
editor: markdown
---

## LVM

> en cours
{.is-warning}

### ajout d'un espace non alloué à un *lv* existant

avec la commande `vgs`, vérifier si de l'espace non alloué est disponible:

```bash
$ vgs
  VG     #PV #LV #SN Attr   VSize   VFree  
  datavg   1   2   0 wz--n- 836.62g 646.62g
  rootvg   1   2   0 wz--n- 135.12g  <1.50g
```

Dans cet exemple on constate que sur `datavg` on utilise 190Go alors que 646Go est encore disponible.

Il est alors possible d'étendre un *lv* existant. On peut lister les *lv* avec `lvs` :

```bash
sudo lvs
  LV         VG     Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  postgreslv datavg -wi-ao---- 150.00g                                                    
  rootlv     rootvg -wi-ao---- 110.00g                                                    
  swaplv     rootvg -wi-ao----  23.62g                                                    
```

On peut maintenant étendre le *lv* `postgreslv` avec la commande `lvextend`. Cette commande nécessite le chemin du *lv*, pour cela on utilise `lvdisplay` : 

```bash
# on filter avec le vg dans lequel se trouve le lv
$ lvdisplay datavg
  --- Logical volume ---
  LV Path                /dev/datavg/postgreslv
  LV Name                postgreslv
  VG Name                datavg
  LV UUID                aKyoE3-kZU2-HX97-duDf-OfqU-7j24-vZ4Xcj
  LV Write Access        read/write
  LV Creation host, time kiaepqdb1.host.coreye.net, 2017-05-22 14:50:19 +0200
  LV Status              available
  # open                 1
  LV Size                150.00 GiB
  Current LE             38400
  Segments               3
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:2

# on récupère l'information LV Path
# ici on ajoute par exemple 50Go
$ lvextend /dev/datavg/postgreslv -L +50g

# il faut aussi redimensionner la partition
$ resize2fs /dev/datavg/postgreslv
```



### ajout d'une partition à un lv existant

```bash
$ lsbk
sda                 8:0    0   50G  0 disk 
├─sda1              8:1    0    1G  0 part /boot
└─sda2              8:2    0   15G  0 part 
  ├─rootvg-rootlv 253:0    0 13.4G  0 lvm  /
  └─rootvg-swaplv 253:1    0  1.6G  0 lvm  [SWAP]

```

ici on voit qu'on dispose de 2 partitions attachées à 2 *logical volume* (lv) mais qu'on utilise seulement 16Go sur 50go.

1. créer la partition avec le reste

```bash
$ fdisk /dev/sda
Welcome to fdisk (util-linux 2.23.2).

Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.


Command (m for help): n
Partition type:
   p   primary (2 primary, 0 extended, 2 free)
   e   extended
Select (default p): p
Partition number (3,4, default 3): 
First sector (33554432-104857599, default 33554432): 
Using default value 33554432
Last sector, +sectors or +size{K,M,G} (33554432-104857599, default 104857599): 
Using default value 104857599
Partition 3 of type Linux and of size 34 GiB is set

Command (m for help): w
The partition table has been altered!

Calling ioctl() to re-read partition table.

WARNING: Re-reading the partition table failed with error 16: Device or resource busy.
The kernel still uses the old table. The new table will be used at
the next reboot or after you run partprobe(8) or kpartx(8)
Syncing disks.
```

Ici on croit qu'on a tout cassé mais `partprobe` corrige le souci.

```bash
$ partprobe
$ lsblk
NAME              MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda                 8:0    0   50G  0 disk 
├─sda1              8:1    0    1G  0 part /boot
├─sda2              8:2    0   15G  0 part 
│ ├─rootvg-rootlv 253:0    0 13.4G  0 lvm  /
│ └─rootvg-swaplv 253:1    0  1.6G  0 lvm  [SWAP]
└─sda3              8:3    0   34G  0 part 
sr0                11:0    1 1024M  0 rom  
```

2. créer le *physical volume* (pv)

```bash
$ pvcreate /dev/sda3
  Physical volume "/dev/sda3" successfully created.
```

3. ajouter le *pv* à son *virtual group* (vg) déjà existant

on vérifie le nom du *vg* et du *pv* :

```bash
$ vgdisplay
  --- Volume group ---
  VG Name               rootvg
  System ID             
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  3
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                2
  Open LV               2
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <15.00 GiB
  PE Size               4.00 MiB
  Total PE              3839
  Alloc PE / Size       3839 / <15.00 GiB
  Free  PE / Size       0 / 0   
  VG UUID               Q83BVj-XLOW-JwHc-YaoZ-VhHH-mrGM-NIc4GX
$ pvdisplay
sudo pvdisplay
  --- Physical volume ---
  PV Name               /dev/sda2
  VG Name               rootvg
  PV Size               <15.00 GiB / not usable 3.00 MiB
  Allocatable           yes (but full)
  PE Size               4.00 MiB
  Total PE              3839
  Free PE               0
  Allocated PE          3839
  PV UUID               KTU3Io-H3gt-3huq-k5IA-jYPC-QP73-zB1Cqq
   
  "/dev/sda3" is a new physical volume of "34.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sda3
  VG Name               
  PV Size               34.00 GiB
  Allocatable           NO
  PE Size               0   
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               9hBkVq-4Um5-aI5q-mrVq-s2sL-JJgh-G2bpnT
```

On va donc étendre `rootvg` avec `/dev/sda3`
```bash
$ vgextend rootvg /dev/sda3
```

4. on étend le *logical volume* avec la totalité du *pv*

on récupère le chemin du *lv* :

```bash
$ lvdisplay
  --- Logical volume ---
  LV Path                /dev/rootvg/rootlv
  LV Name                rootlv
  VG Name                rootvg
  LV UUID                dKdaDz-sTeu-M052-tSxo-XSpS-8R2m-xwe1Ie
  LV Write Access        read/write
  LV Creation host, time localhost, 2018-03-16 15:44:21 +0100
  LV Status              available
  # open                 1
  LV Size                13.39 GiB
  Current LE             3429
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:0

```

on étend avec tout l'espace du *pv*

```
$ lvextend /dev/rootvg/rootlv /dev/sda3
  Size of logical volume rootvg/rootlv changed from 13.39 GiB (3429 extents) to 47.39 GiB (12132 extents).
  Logical volume rootvg/rootlv successfully resized.
```

5. on étend la partition avec tout l'espace disponible

```bash
$ resize2fs /dev/rootvg/rootlv
resize2fs 1.42.9 (28-Dec-2013)
Filesystem at /dev/rootvg/rootlv is mounted on /; on-line resizing required
old_desc_blocks = 2, new_desc_blocks = 6
The filesystem on /dev/rootvg/rootlv is now 12423168 blocks long.
```
