---
title: Tiny Tiny RSS
description: Tiny Tiny RSS est un lecteur de flux RSS libre que j'héberge sur https://rss.valvin.fr. Si vous souhaitez un compte, n'hésitez pas à me contacter.
published: true
date: 2022-11-20T15:12:00.161Z
tags: libre, logiciels, rss, flux
editor: markdown
dateCreated: 2020-04-12T13:01:01.904Z
---

- [Site officiel](https://tt-rss.org/)
- [Dépôt de source *instance gitea*](https://git.tt-rss.org/git/tt-rss/)
- [Support *au travers du forum discourse*](https://discourse.tt-rss.org/)
{.links-list}

__Tiny Tiny RSS__ (tt-rss) est un lecteur de flux RSS / ATOM. Le principe est simple, la plupart des sites publie un lien répertoriant les derniers billets, news dans un fichier xml au format RSS ou ATOM. *tt-rss* va consulter l'ensemble des liens/flux auxquels on est abonné et consituer un fil d'information avec ces différents articles. Il est possible d'organiser ces flux dans différentes catégories.

Il est possible de consulter ces flux RSS à l'aide de l'interface web ou au travers d'une application pour *smartphone*. Pour ma part j'utilise celle disponible sur [F-Droid](https://f-droid.org/fr/packages/org.ttrssreader/) qui est un *fork* de celle développée par le mainteneur du projet.

## Configuration

Pour ma part, j'utilise le thème *feedly* qui je trouve est fort sympathique à utiliser.

Il est possible de partager certains articles avec l'option partage. Cela permet de générer un flux RSS que j'ai lié à mon compte Friendica qui les publie automatiquement.

### Flux RSS partagé

Il est possible de partager certains articles avec une note. Ceux-ci sont ensuite disponible au travers d'un flux RSS qui peut-être activé au travers d'un lien *généré* ([doc](https://tt-rss.org/wiki/GeneratedFeeds))

![rss_articles_partagees.png](/logiciels-libres/ttrss/rss_articles_partagees.png)

Pour ma part je l'utilise dans [Friendica](/logiciels-libres/friendica) ce qui me permet de publier directement sur mon profil les aricles que je souhaite partager.

### Plugins

#### af_proxy_http

Afin que le client n'ait pas a télécharger les médias sur internet, ce plugin permet que le serveur télécharge les médias et lui délivre.

Par défaut ce plugin ne le fait que pour les images sur http mais dans son paramétrage, il est possible de l'activer pour la totalité des médias.

![af_proxy_http.png](/logiciels-libres/ttrss/af_proxy_http.png)

## Installation

### Containerisation
Comme pour l'ensemble de mes services, j'utilise un container docker. J'ai chosi l'image proposée par [x86dev](https://github.com/x86dev/docker-ttrss) qui convient pour mon cas d'usage.

Il faut savoir que le mainteneur du projet fonctionne avec des *rolling-update* c'est à dire que la version *stable* est la version présente sur la branche *master*. Il n'y a donc pas de versions comme on peut en avoir l'habitude. C'est pour cela que j'utilise mon propre dépot que je mets moi même à jour afin de "simuler" des versions: https://framagit.org/valvin/ttrss.git

L'image docker que propose x86dev permet de "bloquer" la version à un certains commit. Sinon celle-ci se met à jour à intervalle régulier. Cela peut être problématique dans certains cas, notamment lors des mises à jour de schémas qui nécessite une action manuelle ou lors des changements *violents*. Un changement de ce type a eu lieu lors de la refonte de l'interface web notamment ou les thèmes existants n'étaient plus compatible.

Voici le *playbook* Ansible que j'utilise pour déployer tt-rss :

```yml
---
- hosts: podman-apps.lxd
  vars:
    podman_user: podman
    app_name: ttrss
    app_image: docker.io/x86dev/docker-ttrss
    app_version: "latest"
  become_user: "{{ podman_user }}"
  tasks:
    - name: "Create {{app_name}} pods and unit"
      become: yes
      containers.podman.podman_container:
        name: "{{ app_name }}"
        image: "{{ app_image }}:{{ app_version }}"
        state: present
        env:
          TTRSS_PORT: "8080"
          TTRSS_SELF_URL: "https://rss.valvin.fr"
          TTRSS_THEME_RESET: "0" 
          TTRSS_REPO_URL: "https://framagit.org/valvin/ttrss.git"
          DB_PORT: "5432"
          DB_PORT_5432_TCP_ADDR: "database.lxd"
          DB_PORT_5432_TCP_PORT: "5432"
          DB_PASS: "{{ ttrss.db_password }}"
        publish:
          - "8083:8080"
        generate_systemd:
          path: "/home/{{ podman_user }}/.local/share/systemd/user"
    - name: Enable and restart systemd {{ app_name }} Service
      become: yes
      ansible.builtin.systemd:
        name: "container-{{app_name}}"
        daemon_reload: yes
        enabled: yes
        state: started
        scope: user
```
