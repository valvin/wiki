---
title: Échecs
description: notes autour de mon apprentissage aux échecs
published: true
date: 2022-11-20T18:14:30.656Z
tags: jeux, loisirs, échecs
editor: markdown
dateCreated: 2021-11-14T14:24:27.873Z
---

# Échecs

### Sites

| Site | Usage | Lien | commentaires |
| - | - | - | - |
| Lichess | parties en ligne, tactiques, études, exercices | [site](https://lichess.org), [profil](https://lichess.org/@/valvin1) | gratuit, Libre |
| Listudy | tactiques ( dont à l'aveugle), études, exercices ouvertures | [site](https://listudy.org/) | gratuit, Libre |

