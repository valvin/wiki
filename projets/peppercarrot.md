---
title: Pepper&Carrot - une bande-dessinée libre
description: J'ai la chance de modestement contribuer au projet de David Revoy qui raconte l'histoire d'une sorcière nommée Pepper et de son chat Carrot
published: true
date: 2020-04-18T06:32:33.218Z
tags: libre, opensource, pepper&carrot, bd
---

J'ai découvert le projet de David Revoy me semble t-il grâce à un article de Framasoft. Depuis toujours, j'ai une attirance pour le dessin et la bande-dessinée. Je me suis donc jeté sur l'occasion.

[Pepper&Carrot](https://www.peppercarrot.com/) a été publié pour la première fois en mai 2014. Derrière ce premier épisode, l'idée folle d'apporter une autre vision de l'édition de la bande-dessinnée : 

## Le financement
Ce projet serait financé, non pas par un éditeur mais directement pas ces lecteurs. A chaque épisode, grâce à des plateformes comme *Patreon*, les lecteurs qui le souhaitent, réménurent l'auteur du montant qu'ils ont choisi. Lors de l'épisode 32, il y avait __1121__ mécènes. 

[Page pour devenir mécène](https://www.peppercarrot.com/fr/static12/donate) 

## Le libre comme philosophie

### Des outils libres
L'auteur, David Revoy, est un grand promotteur / contributeur des solutions Libres. Il a donc fait le choix de n'utiliser que des outils Libre / Opensource pour la réalisation de ce projet: 

* En poste de travail, on retrouvera __*Linux*__ au travers de la distrution *Kubuntu*. 
* Pour la réalisation du graphisme, le logiciel [__*Krita*__](https://krita.org/fr/)
* Pour la mise en page et réalisation des dialogues [__*Inkscape*__](https://inkscape.org/fr/)
* Pour la réalisation d'une édition papier de la bande-dessinée [__*Scribus*__](https://www.scribus.net/)

### La mise à disposition des sources
Bien entendu, tout le travail réalisé est disponible sur le [site](https://www.peppercarrot.com/en/static6/sources) et sur le [dépôt git](https://framagit.org/peppercarrot) du projet.

### Une licence ouverte CC-BY

C'est probablement le choix le plus important du projet et qui a sucité un peu de controverse chez les autres auteurs de bande-dessinée. La licence [Creative Commons CC-BY](https://creativecommons.org/licenses/by/4.0/)

Cette licence est très ouverte, elle autorise la réutilisation totale du travail réalisé, sans contre-partie obligatoire. La seule obligation est de mentionner l'attribution du travail.

Cela signifie que n'importe quel éditeur peut s'il le souhaite éditer la bande-dessinée du moment qu'il lui attribue le travail. Certains l'on fait, comme Glénat mais tente cependant de rentrer dans la philosophie du projet en devenant mécène du projet au même titre qu'un lecteur. Techniquement, il n'a pas plus de poids qu'un autre lecteur.


## La traduction

Je ne sais pas si c'était le souhait initial, mais grâce à la philosophie du projet et au travail d'automatisation mis en place. Au moins un épisode est traduit dans 51 langues, un tableau représentatifs est [disponible ici](https://www.peppercarrot.com/en/static6/sources&page=translation)

## Mes contributions

### Réalisation d'un épisode

Comme d'autres fans/contributeurs, nous avons la chance de participer à l'ensemble de process de la réalisation d'un épisode : la relecture du scénario, le story-board, les dialogues...

Sur les derniers épisodes, ceux-ci étant écris en anglais, je participe à la traduction de la version anglaise.

### Outils

(TODO)