---
title: RFLink - passerelle 433MHz
description: 
published: true
date: 2022-08-31T19:54:08.999Z
tags: domotique, 433mhz, rflink
editor: markdown
dateCreated: 2022-08-31T19:54:08.999Z
---

Matériel dispo sur nodo-shop.nl

pour se connecter directement depuis le PC on peut utiliser `cutecom`

RFlink communique en 57600bauds

Par défaut le matériel ne détecte pas tous les signaux. il existe un mode debug qui permet de mieux les voir.

Il également possible de faire *apprendre* à RFLink certains protocole avec `RFFind`

Pour passer en mode debug: `10;RFDEBUG=ON;`

Référence du protocole: https://www.rflink.nl/protref.php

La commande du volet semble être liée à la puce [EV1527](http://aitendo3.sakura.ne.jp/aitendo_data/product_img/wireless/315MHz-2012/RX315-HT48R/EV1527.pdf) souvent utilisée dans les télécommandes bas couts.

En mode debug on obtient des choses du genre :

```
20;AA;EV1527;ID=0e7xxx;SWITCH=yy;CMD=ON;
```

pour envoyer le signal équivalent, il faut retirer certains éléments:

```
10;EV1527;0e7xxx;yy;ON;
```

si on teste plusieurs fois le même bouton, l'ID et le Switch changent.

Le raspberry est source de perturbations pour la réception des signaux.

pour téléverser le firmware sans windows:

```
avrdude -v -p atmega2560 -c stk500 -P /dev/ttyACM0 -b 115200 -D -U flash:w:/home/vincent/Downloads/rflink/RFLink.cpp.hex:i
```
