---
title: BangleJS - montre connectée opensource
description: Notes sur la montre connectée BangleJS d'Espruino
published: true
date: 2021-04-04T18:32:31.263Z
tags: montre, javascript, iot, geek
editor: markdown
dateCreated: 2021-04-03T13:37:28.444Z
---

La montre bangleJS est du matériel Opensource. Elle dispose des fonctions suivantes:

* horloge
* pédomètre
* rythme cardiaque
* GPS

## Les différentes applications

Les différentes applications se décomposent en trois groupes : horloges, widgets et apps. On les retrouve [ici](https://banglejs.com/apps/)

### Les horloges

C'est l'application qui est lancée au démarrage. Et comme son nom l'indique affiche l'heure mais aussi d'autres éléments que l'on peut paramétrer.
L'horloge par défaut se choisit dans les paramètres.

### Les widgets

Les widgets permettent d'afficher une information dans la barre haute de l'écran comme le niveau de batterie, la connectivité bluetooth...
Cela permet également de lancer une application en tâche de fond.

### Les apps

Comme son nom l'indique il s'agit d'une application. Elles sont lancées depuis le lanceur d'applications et fonctionne individuellement mais reste active jusqu'à ce qu'on en sorte.


## Quelles applications ?

### Pedomètre

Il y a deux *widgets* pour les applications:

* Active Pedometer: cette application semble être la plus utilisée. elle contient beaucoup de paramétrage. Je n'ai pas encore trouvé le bon paramétrage pour avoir quelque chose de correct.
* Pedometer widget: prise en compte les différentes horloges mais ne peut pas être paramétrée. Le nombre de pas est très élevé (trop sensible).

## Mise à jour du firmware

1. Télécharger le firmware sur le site d'espruino en sélectionnant le type de carte: [ici](http://www.espruino.com/Download)
2. Passer la montre en DFU : appuie sur BTN1 + BTN2, la montre rédémarre, laisser appuyer jusqu'à l'indiquation sur la montre. Si la montre vibre, on a attendu trop longtemps
3. Avec un téléphone android et l'application NRF Toolbox, cliquer sur DFU, choisir le fichier télécharger puis la montre dont le nom est `DFUTag`
