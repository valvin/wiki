---
title: Vim - éditeur de texte
description: Vim est un éditeur de texte qui dispose d'un mode d'édition spécifique qui leur permet d'avoir une meilleure productivité avec le moins de mouvements possibles
published: true
date: 2022-08-24T10:58:21.160Z
tags: libre, logiciel, editeur, vi, vim
editor: markdown
dateCreated: 2020-07-29T18:44:55.732Z
---

> Brouillon - rédaction en cours
{.is-warning}

Utilisateur de `vim` depuis plusieurs années, j'ai l'impression que l'on peut apprendre tous les jours quelque chose de nouveau (ou ré-apprendre ;-) ).

J'ai pu bien appréhender `vim` grâce au libre en ligne *vim pour les humains* (disponible [ici](https://vimebook.com/fr)). Grâce à ce livre, j'ai mieux compris la philosophie, même si je pense qu'il y quelques parties qui on pris un petit coup de vieux.

Je sais qu'il y a la guerre avec `emacs` mais comme dans beaucoup de cas, le meilleur outils est celui qu'on sait utiliser! Pour moi c'est `vim` et je pense avoir beaucoup à apprendre.

## Les modes d'éditions

L'éditeur a la spécificité d'avoir deux modes d'éditions : le mode normal (qui n'en ai pas un) et le mode d'insertion.

La philosophie derrière ces modes d'éditions est de pouvoir réaliser tout ce dont on a besoin en bougeant le moins possible les mains.

### Le mode normal

C'est le mode par défaut, celui où quand on tape, rien ne s'écrit! Ce mode permet de parcourir le fichier, sélectionner dans différents modes, faire des recherches, taper des commandes et même parait-il sortir de l'éditeur...

Pour la navigation, on peut le faire comme avec les éditeurs habituels, les flèches, mais en réalité, cela va à l'encontre de la philosophie. Il est préférable d'utiliser les touches `h`, `j`, `k`, `l`. La raison est simple, on peut parcourir le fichier dans position de repos, sans bouger sa main gauche.

Pour passer en mode commande, par défaut, c'est avec la touche `:` et parait-il que la première commande à savoir n'est pas `:q` qui permet de sortir (si on a pas modifié le fichier, sinon `:wq` pour enregistrer ou `:q!` pour quitter sans enregistrer). La commande à connaître est `:help` car quoi qu'en dise, `vim` dispose d'une aide intégrée très bien fournie.

Pour changer de mode et commencer à écrire, il faut taper `i` et pour revenir en mode normal `Echap`.

### Le mode insertion

On a l'impression de retrouver un éditeur de texte normal, on peut donc commencer à écrire ou modifier le fichier. Ceci dit, il y a quand même des raccourcis par défaut ou que l'on peut implémenter. Par exemple `Ctrl+P` en mode insertion, permet de faire de l'autocomplétion avec les mots déjà présent dans le fichier.

## Quelques commandes

La plupart des commandes utilisent des touches dont la lettre correspond à l'action. Quand c'est le cas, je l'indique entre paranthèse et/ou en gras.

### Commande de déplacement

En mode normal :

| touche(s)  | action  |
|---|---|
| `h` | déplacement vers la gauche |
| `j` | déplacement vers le bas |
| `k` | déplacement vers le haut |
| `l` | déplacement vers la droite |
| `w` | déplacement au début mot suivant (__word__)|
| `b` | déplacement au début du mot précédent (__before__)|
| `e` | déplacement à la fin du mot (__end__) |
| `^` | déplacement en début de ligne |
| `$` | déplacement en fin de ligne |
| `ctrl+o` | revenir à la position précédente (__origin__) |
| f<lettre> | se déplace à la prochaine occurence de <lettre> à partir du curseur|
 

### Commandes de sélection

On parle de mode *visuel* que l'on peut activer de trois façon depuis le mode normal :

| touche(s) | action |
| --- | --- |
| `v` | sélection à partir du caractère où l'on se trouve. Une fois activé, il est possible d'augmenter ou diminuer en utilisant les commandes de déplacements. (__visual__) |
| `shift+v` | sélection de la ligne courante et possibilités d'en sélectionner avant/après en se déplaçant|
| `ctrl+v` | sélection du caractère mais avec la possibilité de se déplacer dans tous les sens. c'est le mode bloc|

Une fois le mode de sélection activé, on peut également l'associé aux commandes de déplacements, par exemple : `v$` sélectionnera le texte du curseur jusque la fin de ligne.
  
Il est également possible de sélectionner un paragraphe complet avec `vip` (visual mode in paragraph).

### Commandes d'actions

Une fois sélectionné, par exemple `v $` (sélection du curseur jusqu'à la fin de la ligne), il est possible d'éxécuter une action sur celle-ci:

| touche(s) | action |
| --- | --- |
| `x` | supprime la sélection |
| `d` | supprime la sélection (__delete__) |
| `c` | supprime  la sélection et passe en mode insertion (__change__)|
| `y` | copie la sélection (__yank__)|
| `p` | insert après le curseur la dernière sélection copié (ou supprimé) (__paste__)|
| `"+y` | copie dans le registre presse-papier du système |
| `"*y` | copie dans le second registre presse-papier du système |
| C | supprime jusqu'à la fin de la ligne et place en mode insertion |
| D | supprime jusqu'à la fin de la ligne |

Il est possible également de faire des actions sans sélection :

| touche(s) | action |
| --- | --- |
| `yy` | copie la ligne |
| `dd` | supprime la ligne |
| `di"` | supprime le contenu entre `"`, peut-être remplace par `(`, `[` ... (__delete in "__)|
| `ci"` | supprime et passe en mode insertion le contenu entre `"`, peut-être remplace par `(`, `[` ... (__change in "__)|

### Commandes utiles

| touche(s) | action |
| --- | --- |
| `:put=strftime('%FT%T%z')` | insert la date au format précisé |
| `r !date` | insert le contenu du résultat de la commande shell sur la ligne en dessous |
| `gq` | formate la sélection en fonction du nombre de caractère par ligne que l'on peut définir par `set tw=80` (et active le retour à la ligne automatique) |
