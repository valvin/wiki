---
title: RPM - Redhat Package Manager
description: Quelques infos autour des packages RPM que l'on retrouve sur RedHat, CentOS et Fedora
published: true
date: 2020-07-08T12:21:00.418Z
tags: linux, rpm, redhat, fedora, centos
editor: markdown
---

## RPM

> à completer
{.is-warning}

Le [RPM](https://fr.wikipedia.org/wiki/RPM_Package_Manager) est un système de paquets que l'on retrouve entre autres sur les distributions RedHat, CentOS et Fedora.


Les *RPM* sont construits sur la base d'un fichier *spec* qui permet à partir de sources de construire un livrable pour la plateforme cible. Il permet donc de déposer ces livrables mais également d'éxécuter différentes actions avant / après l'installation / mise à jour / suppression.

## Construction de RPM

Le sens d'éxécutions des *scriplets* (%pre , %post...) est très spécifique. Par exemple lors d'une mise à jour, le `%postun` du paquet précédent est éxécuté après le `%post` du nouveau paquet.

Ordre d'éxécution ( [source](https://docs.fedoraproject.org/en-US/packaging-guidelines/Scriptlets/) )
```
    %pretrans of new package

    %pre of new package

    (package install)

    %post of new package

    %triggerin of other packages (set off by installing new package)

    %triggerin of new package (if any are true)

    %triggerun of old package (if it’s set off by uninstalling the old package)

    %triggerun of other packages (set off by uninstalling old package)

    %preun of old package

    (removal of old package)

    %postun of old package

    %triggerpostun of old package (if it’s set off by uninstalling the old package)

    %triggerpostun of other packages (if they’re setu off by uninstalling the old package)

    %posttrans of new package


```

Dans un *scriptlet* il est possible de savoir dans quel mode on se situe, installation / mise à jour / suppression.

Pour cela, il faut vérifier la valeur de la variable `$1` : 
| scriplet| installation | mise à jour | suppression |
| ---      | --- | --- | --- |
|%pretans  | 0   | 0   | N/A |
|%pre      | 1   | 2   | N/A |
|%post     | 1   | 2   | N/A |
|%preun    | N/A | 1   | 0   |
|%postun   | N/A | 1   | 0   |
|%posttrans| 0   | 0   | N/A |
