---
title: Linux - find
description: commande find : recherche des fichiers à partir d'une arborescence et permet de les manipuler
published: true
date: 2020-07-23T12:23:04.108Z
tags: linux, commande, find, fichier
editor: markdown
---

## Find

```bash
# Recherche de fichier par son nom de type wildcard à partir du chemin courant
find . -name *.xml
# Recherche des fichiers modifiés il y a de 30jours
find . -type f -mtime +30
# Même commande mais en supprimant ces fichiers
find . -type f -mtime +30 -exec rm {} \;
```