---
title: Le stockage sous linux
description: 
published: true
date: 2020-09-04T13:27:13.839Z
tags: linux, disque, stockage, df, du, scsi, lsblk
editor: markdown
---


> Pour LVM, [cliquez-ici](/linux/lvm)
{.is-info}

## Visualiser les disques et partitions

La commande `lsblk` permet de visualiser les disques et partitions du système et le *logical volume* (lvm) auquel la partition est attachée.

```bash
$ lsblk
NAME                     MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sr0                       11:0    1 1024M  0 rom  
sda                        8:0    0   16G  0 disk 
├─sda1                     8:1    0    1G  0 part /boot
└─sda2                     8:2    0   15G  0 part 
  ├─rootvg-swaplv (dm-0) 253:0    0  3.6G  0 lvm  [SWAP]
  └─rootvg-rootlv (dm-1) 253:1    0 27.3G  0 lvm  /
sdb                        8:16   0   16G  0 disk 
└─sdb1                     8:17   0   16G  0 part 
  └─rootvg-rootlv (dm-1) 253:1    0 27.3G  0 lvm  /

```

## Prise en compte d'un nouveau disque

Afin de prendre un nouveau disque, on peut envoyer la commande permettant d'actionner un scan des bus scsi.

```
echo "- - -" > /sys/class/scsi_host/host<id>/scan
```

