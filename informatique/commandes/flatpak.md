---
title: Flatpak
description: flatpak est un système de paquettage d'application dans un environnement contrôlé (bac à sable)
published: true
date: 2022-07-25T06:26:16.489Z
tags: flatpak
editor: markdown
dateCreated: 2022-07-25T06:13:47.649Z
---

## divers

```bash
# recherche
flatpak search <mot_clé>
# installation <dépot> = flathub dans 90% des cas
flatpak install <dépot> <mon.paquet.que.jai.vu>
# suppression
flatpak uninstall <mon.paquet.que.je.veux.enelver>
# liste les applications et leur taille
flatpak --columns=app,name,size,installation list
```
## gestion des dépots
### espace de sotckage

```bash
# retire les paquets non utilisés
flatpak uninstall --unused
# vérifie toutes les dépendances et supprime celles inutiles
sudo flatpak repair
```