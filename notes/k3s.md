## Stockage

## Traefik

### Transfert de l'IP du client

La configuration par défaut ne permet pas d'obtenir l'ip réelle du client.

Pour cela il faut modifier le service traefik pour ajouter l'option `externalTrafficPolicy: Local`

Malheureusement elle est rechargée par défaut au démarrage si on modifie via `kubectl -n kube-system edit svc traefik`

La modification du fichier `/var/lib/rancher/k3s/server/manifests/traefik.yaml` qui est l'instanciatino du chart helm peut se faire en ajoutant `externalTrafficPolicy: Local` dans la section `valuesContent`

Cependant cette modifiction sera écrasée si l'option `--no-deploy traefik` n'est pas ajoutée au service.

### Mise en place ACME

C'est sensé fonctionner avec la version v1.7.19 mais non recommandée car les certificat sont stockés dans des volumes alors que certmaanger utilise des Secret.
https://opensource.com/article/20/3/ssl-letsencrypt-k3s
problème lors de la création du CertificateIssuer

* Liens :
  - [ticket sur le souci x-real-ip](https://github.com/rancher/k3s/issues/1652)
  - [chart traefik](https://github.com/helm/charts/tree/master/stable/traefik)
  - [persitance + acme](https://nicklas.wiegandt.eu/2020/08/02/use-k3os-traefik-letsencrypt.html)


Notes migration :
Postgres : ajout listen_adresses sur l'ip privée et du sous-réseau 10.128.1.0/28 dans pg_hba.conf


----

Incident 2020/12/17:

* montée de version k3s : 
  * suppression des paramètres du services systemd.
  * suppression de la configuration du chart helm traefik (option --no-deploy traefik manquante)
  * impossibilité de regénerer les cerificats let's encrypt. plusieurs tests ont posé problème, le replicas à 2 et le fait qu'on est un pvc. la solution finale est d'avoir supprimé le fichier acme.json dans le pvc.

* à faire:
  * configurer k3s pour que les options du service soit persistente
  * faire en sorte que le charte helm soit persistent
  * paramétrer un kv sur traefik pour le stockage acme

`/var/lib/rancher/k3s/server/manifests/traefik.yaml`

```yaml
apiVersion: helm.cattle.io/v1
kind: HelmChart
metadata:
  name: traefik
  namespace: kube-system
spec:
  chart: https://%{KUBERNETES_API}%/static/charts/traefik-1.81.0.tgz
  valuesContent: |-
    replicas: 1

    rbac:
      enabled: true
    ssl:
      enabled: true
      enforced: false
    tlsMinVersion: VersionTLS12
    acme:
      enabled: true
      staging: false
      email: "webmaster@valvin.fr"
      challengeType: "http-01"
      httpChallenge:
        entrypoint: http

    persistence.enabled: true
    metrics:
      prometheus:
        enabled: true
    resources:
      requests:
        cpu: 100m
        memory: 64Mi
      limits:
        cpu: 300m
        memory: 256Mi
    kubernetes:
      ingressEndpoint:
        useDefaultPublishedService: true
    image: "rancher/library-traefik"
    externalTrafficPolicy: Local
    tolerations:
      - key: "CriticalAddonsOnly"
        operator: "Exists"
      - key: "node-role.kubernetes.io/master"
        operator: "Exists"
        effect: "NoSchedule"
```

`etc/systemd/system/k3s.service`
```
ExecStart=/usr/local/bin/k3s \
    server \
    --no-deploy traefik \
    --default-local-storage-path /mnt/k3s-data \
```
