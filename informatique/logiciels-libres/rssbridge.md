---
title: RSS Bridge - retrouver vos sites dans votre lecteur de flux
description: RSS Bridge permet de retrouver dans votre lecteur de flux, les sites qui ne proposent plus de flux rss/atom
published: true
date: 2021-03-21T13:20:43.227Z
tags: libre, rss, atom, bridge, outil
editor: markdown
dateCreated: 2021-03-21T13:20:43.227Z
---

- [Dépôt de source](https://github.com/RSS-Bridge/rss-bridge)
- [Wiki](https://github.com/RSS-Bridge/rss-bridge/wiki)
- [Mon instance (usage limité)](https://rssbridge.valvin.fr)
{.links-list}


__RSS Bridge__ propose de créer des flux *Atom* de sites *Facebook*, *Twitter*, *Instagram*... cela permet de suivre par exemple des pages Facebook de certains commerces qui n'utilise (malheureusement) que ce type de support pour communiquer.

Déployer sa propre instance est relativement simple. Voici ma configuration sur mon cluster k3s:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: rssbridge
  labels:
    app: rssbridge
spec:
  replicas: 1
  selector:
    matchLabels:
      app: rssbridge
  template:
    metadata:
      labels:
        app: rssbridge
    spec:
      containers:
        - name: rssbridge
          image: rssbridge/rss-bridge:stable
          resources:
            requests:
              memory: "32Mi"
              cpu: "10m"
            limits:
              memory: "128Mi"
              cpu: "200m"
          ports:
            - containerPort: 80
```

## Mon usage

### Facebook

Je suis contre ce réseau social mais malheureusement on retrouve beaucoup de personnes, commerces, association qui ne communique qu'au travers de ce réseau.

A partir de l'ID de la page, on parvient à obtenir un flux Atom et continuer de suivre l'actualité dans son lecteur de flux.

### Youtube

A partir de l'ID d'un utilisateur ou d'un chaine, on peut obtenir la liste des dernières vidéos publiées dans un flux.

### Autres modules

Il est possible de d'avoir bien d'autres sites au travers d'un flux. N'hésitez pas à découvrir et ajouter de nouveaux flux.
