---
title: Home Assistant
description: home assistant est un logiciel de gestion pour sa domotique proposant un grand nombre d'intégrations et de scénarios
published: true
date: 2022-11-21T20:58:20.821Z
tags: domotique, homeassistant
editor: markdown
dateCreated: 2022-11-21T20:57:51.264Z
---

# Le projet

Projet permettant de se connecter à de nombreux objets connectés pour la domotique de sa maison.

* [Site officiel](https://www.home-assistant.io/)

## intégration utilisée

* [Frigate](https://github.com/blakeblackshear/frigate) : pour la détection vidéo
* [Drayton Wiser](https://github.com/asantaga/wiserHomeAssistantPlatform) : pour la gestion des chauffage
* Matrix : pour la notification

## exemples de scénarios

### détection de personnes

```yaml
alias: Notifications - détection personne frigate
description: ""
trigger:
  - platform: mqtt
    topic: frigate/events
condition:
  - condition: template
    value_template: "{{ trigger.payload_json[\"after\"][\"label\"] == \"person\" }}"
  - condition: template
    value_template: "{{ trigger.payload_json[\"type\"] == \"end\" }}"
action:
  - service: notify.matrix_notify
    data:
      message: Présence maison détectée @valvin
      data:
        images:
          - >-
            /media/frigate/clips/{{
            trigger.payload_json["after"]["camera"]}}-{{trigger.payload_json["after"]["id"]}}.jpg
mode: single
```

### alertes gelées

```yaml
alias: Alerte gelée
description: ""
trigger:
  - platform: time
    at: "19:00:00"
condition:
  - condition: template
    value_template: "{{ state_attr(\"weather.xxxx\",\"forecast\")[1].templow < 2}}"
action:
  - service: matrix.send_message
    data:
      message: >-
        Attention gelée demain, température basse prévue: {{
        state_attr("weather.xxxx","forecast")[1].templow }}°C @valvin
      target: "#xxxx:matrix.org"
mode: single
```