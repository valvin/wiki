---
title: Wiki.js
description: Wiki.js le moteur de ce wiki
published: true
date: 2021-09-10T19:06:43.049Z
tags: opensource, logiciels, wiki, wikijs
editor: markdown
dateCreated: 2020-04-09T11:55:02.823Z
---

- [Site officiel](https://wiki.js.org/)
- [Dépôt du projet](https://github.com/Requarks/wiki)
- [Note de version](https://docs.requarks.io/en/releases)
- [Lien de mon wiki](https://wiki.valvin.fr)
{.links-list}


[Wiki.js](https://wiki.js.org/) est le wiki qui permet d'afficher cette page.

J'ai choisi ce système plutôt qu'un [DokuWiki](https://www.dokuwiki.org/dokuwiki) ou un [BookStack](https://www.bookstackapp.com/).

Les raisons sont que *Wiki.js* peut :

* Éditer le contenu en *markdown*
* Synchroniser le contenu du wiki vers un fournisseur de stockage et notamment *Git*
* Éditer depuis l'interface web ou depuis un fournisseur de stockage (*Git* dans mon cas)

Cette page est disponible [ici](https://framagit.org/valvin/wiki/-/blob/master/logiciels-libres/wikijs.md) et cela permet à ceux qui le souhaitent de proposer des modifications et de mon côté à modifier le contenu depuis mon éditeurs de fichiers favoris.


## Syntaxe Markdown

Le moteur *markdown* mis à disposition à quelques spécificités propres à *wiki.js* qui ne sont pas standard.

*Source: [documentation wiki.js](https://docs.requarks.io/en/editors/markdown)*

### La syntaxe standard

On retrouve bien entendu les titres, la mise en gras, italique, les liens, les images... L'information est disponible facilement, pas besoin de tout redétailler.

>[Page wikipédia sur le markdown](https://fr.wikipedia.org/wiki/Markdown)

### La syntaxe spécifique wiki.js

#### Liste de tâches

Syntaxe déjà utilisée dans d'autres système comme Gitlab.

```
- [ ] tâche non réalisée
- [x] tâche réalisée
```

- [ ] tâche non réalisée
- [x] tâche réalisée

#### Les infos, avertissements et alertes

On utilise la syntaxte markdown pour les citations `>`, sur une ou plusieurs lignes. En dessous, on utilise la syntaxe `{.is-<info|warning|danger|success>}`. Cette syntaxe permet d'ajouter une classe CSS sur la citation.

```
> Texte d'information
{.is-info}
```

>Texte d'information
{.is-info}

>Texte d'avertissement
{.is-warning}

>Texte d'alerte
{.is-danger}

>Texte de succès
{.is-success}

#### Liste de liens et grilles

Tout comme pour les infos/alertes, on rajoute une classe CSS mais cette fois-ci à une liste

```
- Premier élément
- Second élément
- Troisième élément
{.grid-list}
```

- Premier élément
- Second élément
- Troisième élément
{.grid-list}

Pour les listes de liens, une petite spécificité est le sous-titre. On utilise la syntaxe suivante pour le lien `[Titre du lien *sous-titre du lien*](http://....)`

```
- [Premier lien *sous-titre du lien*](https://wiki.valvin.fr)
- [Second lien *sous-titre du lien*](https://wiki.valvin.fr)
- [Troisième lien lien *sous-titre du lien*](https://wiki.valvin.fr)
{.links-list}
```

- [Premier lien *sous-titre du lien*](https://wiki.valvin.fr)
- [Second lien *sous-titre du lien*](https://wiki.valvin.fr)
- [Troisième lien lien *sous-titre du lien*](https://wiki.valvin.fr)
{.links-list}


#### Indice & Exposant


Il est possible de mettre en indice un texte comme ~ceci~ . 

Pour cela il faut utiliser `~indice~`. Cela ne fonctionne que pour un mot.

Pour les exposants, comme ^ceci^. On utilise `^exposant^`

#### Taille d'images

Contrairement à la syntaxe standard, il est possible de préciser la taille de l'image avec la syntaxe suivante:

```
![logo de wiki.js](/wikijs-butterfly.svg =100x100)
```

![logo de wiki.js](/wikijs-butterfly.svg =100x100)

Ou pour conserver le ratio, on peut préciser qu'une seule des dimensions : 

```
# on ajuste l'image à 100px en largeur
![logo de wiki.js](/wikijs-butterfly.svg =100x)
# on ajuste l'image à 100px en hauteur
![logo de wiki.js](/wikijs-butterfly.svg =x100)
# on ajuste l'image à 100% de la largeur
![logo de wiki.js](/wikijs-butterfly.svg =100%x)
```

Il est également possible d'aligner les images avec l'ajout d'une classe CSS.

`{.align-right}` `{.align-left}` `{.align-center}` `{.align-abstopright}`

```
![logo wiki.js](/wikijs-butterfly.svg =100x){.align-center}
```

![logo wiki.js](/wikijs-butterfly.svg =100x){.align-center}

----

## Installation

Pour ma part, j'ai choisit d'installer cette instance avec un conteneur *docker* et une base de données *postgresql* qui elle est hébergée sur un serveur virtuel.

Pour la partie docker, n'ayant pas encore de cluster *Kubernetes*, je déploie à l'aide d'*Ansible* avec un playbook correspond à celui-ci :

```yml
---
- hosts: all 
  vars:
    wikijs_version: 2
  tasks:
    - name: container docker wikijs
      docker_container:
        name: wikijsapp
        image: requarks/wiki:{{ wikijs_version }}
        pull: true
        ports:
          - "127.0.0.1:8090:3000"
        state: started
        restart: yes 
        restart_policy: unless-stopped
        env:
          DB_TYPE=postgres
          DB_HOST=x.x.x.x
          DB_PORT=5432
          DB_USER=wikijs
          DB_PASS={{ vault_wikijs_db_pwd }}
          DB_NAME=wikijs
      tags: wikijs
```

Pour exposer le service, celui-ci est disponible via un serveur *nginx* que je n'ai pas encore containerisé. Il fait la terminaison *TLS* et reverse proxy. 

---

## Configuration

### Synchronisation Git

Il est possible de synchroniser le contenu du wiki sur différents types de stockage (local, stockage distant et git). Pour ma part, j'ai choisit *git* et le dépôt de ce wiki se trouve sur [framagit](https://framagit.org/valvin/wiki)

Pour configurer git, dans le menu administration, il est nécessaire d'aller dans `Module / Stockage`.

La configuration est assez intuitive. Pour ma part, j'ai choisi d'utiliser un clé SSH de type `contenu`. Clé que j'ai configurée dans Gitlab comme une *Deploy key* (paramètre dans *CI/CD*) avec une autorisation d'écriture.

![wikijs-git-storage.png](/logiciels-libres/wikijs-git-storage.png =70%x){.align-center}

Une fois configuré, l'action *Import everything* va importer tout le contenu dans le dépôt Git. Pour ma part, j'ai également joué avec les options *Add untracked changes*, *Force sync* avant et j'ai du ensuite faire *Purge local repository* puis *Import everything.

La synchronisation peut être :

- bidirectionnelle: ce qui permet de faire des changements depuis l'interface du wiki ou dans le dépôt git
- téléverser vers la cible: envoie les données du wiki vers git, uniquement
- télécharge de la cible: ajoute les données de git vers le wiki, uniquement

### Recherche

La recherche basique permet de rechercher sur la base des étiquettes mises sur chaque article. Il est possible de rendre la recherche plus performante en utilisant les autres possibilités.

Ici j'ai activé la recherche avec *PostgreSQL*. Pour cela, comme indiqué dans la [documentation](https://docs.requarks.io/en/search/postgres), il faut ajouter l'extension qui permet de déterminer les similarités entre mots :

```sql
CREATE EXTENSION pg_trgm;
```

### Authentification

#### Gitlab

Pour configurer l'authentification gitlab, il est nécessaire de créer une [application](https://gitlab.com/oauth/applications) directement sur gitlab.com ou votre instance. Pour que cela fonctionne il est nécessaire de donner le scope `read_user`. L'url de retour se trouve dans l'interface admin au même endroit ou vous devez saisir le *client id* et le *secret id* `https://<wikisjsdomain.tld>/a/auth`

----










