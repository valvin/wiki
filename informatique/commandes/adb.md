---
title: Adb - Android Debug Bridge
description: liste des commandes utiles pour travailler avec Android en mode debug
published: true
date: 2020-07-29T11:37:13.380Z
tags: commande, adb, android, developpement
editor: markdown
---

ADB est l'outils utilisé lorsque l'on veut intéragir avec un terminal Android (mais pas que). C'est par exemple lui qui est utilisé par l'IDE (Android Studio par exemple), pour déployer / déboguer une application.

Voici une liste des commandes que j'ai souvent utilisées:

## Intéraction avec le démon adb

```bash
# tuer le démon actuel
adb kill-server
# démarrer le serveur
# celui-ci se lance automatiquement lors de l'execution d'uatre commande
adb start-server
# lister les terminaux connectés
# le terminal doit être en mode debug et la clé du serveur accepté sur le terminal
adb devices
# pour faire du débuggage distant
adb tcpip
# puis pour se connecter sur l'IP X.X.X.X
adb connect X.X.X.X:5555
# visualiser les logs
adb logcat
```

## Installation / désinstallation d'application

```bash
# pour installer une nouvelle application
adb install <mon_fichier>.apk
# pour mettre à jour une application
# attention il faut un version code supérieur et la même signature
adb install -r <mon_fichier>.apk
# pour désinstaller un package
adb uninstall <com.mon.package>
```

## Manipulation de fichier

```bash
# copier un fichier du terminal vers le pc
adb pull </chemin/sur/le/terminal> </chemin/sur/le/pc>
# récupérer un fichier du terminal sur le pc
adb push </chemin/sur/le/pc> </chemin/sur/le/terminal>
```

## Execution de commande sur le terminal

```bash
# activer une invite de commande
adb shell
# lancer une commande spécifique
adb shell <la_commande>
```

## Commandes intéressantes

```bash
# pour liste les package installé sur le terminal
adb shell pm list packages -3
# pour obtenir des information sur un package installé
adb shell dumpsys package <com.mon.package>
# ou
adb shell dumpsys package <com.mon.package>
# pour lancer une activité
adb shell am start -n <activity>
# pour lancer une action
adb shell am start -a <action>
# pour simuler des intéraction utilisateur (clavier / écran tactile)
adb shell input keyevent <key_code>
adb shell input tap <x> <y>
adb shell input roll <dx> <dy>
adb shell input swipe <x1> <y1> <x2> <y2>
adb shell input text <text>
```

## Sauvegarde / restauration

```bash
# si l'application le permet dans son manifeste
adb backup -f <mon_fichier>.ab -noapk <com.mon.package>
# pour restaurer
adb restore <com.mon.package>
```

On peut aussi analyser / modifier les données de sauvegardes avec [android backup extractor](https://github.com/nelenkov/android-backup-extractor) ou avec des [commandes systèmes](https://stackpointer.io/mobile/android-adb-backup-extract-restore-repack/372/)

Avec abe, on a des commandes de ce type

```bash
# créer une archive avec les données
java -jar abe.jar unpack </chemin/vers/fichier.ab> </chemin/fichier.tar>
# opération inverse
java -jar abe.jar pack </chemin/fichier.tar> </chemin/fichier.ab>
```

On trouve des choses intéressantes dans le package `com.android.settings` :)

## Autres

```bash
# quand on a plusieurs terminaux on peut en sélectionner par son numéro de série
adb -s <num_serie> <action_souhaitée>
```