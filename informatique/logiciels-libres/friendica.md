---
title: Friendica - réseau social fédéré
description: Friendica est compatible avec un grand nombre d'autre réseau social décentralisé comme Mastodon, Diaspora, Hubzilla...
published: true
date: 2022-11-20T14:39:42.834Z
tags: libre, réseau social, activitypub, fédération, friendica
editor: markdown
dateCreated: 2020-04-11T07:50:30.518Z
---

- [Site officiel](https://friendi.ca/)
- [Dépôt du projet *code source et tickets*](https://github.com/friendica/friendica)
- [~~@valvin@friendica.valvin.fr~~ *mon ex-utilisateur friendica, service fermé suite a des soucis d'administration réccurent*](https://friendica.valvin.fr/profile/valvin/profile)
{.links-list}

Contrairement aux outils comme *Mastodon*, *Friendica* est un réseau social qui priviligie le texte long et la lecture sous forme de discussion. Il s'approche plus de ce que l'on retrouve sur *Diaspora* ou *Facebook*.

La grande force de l'outils et sa compatibilité avec un grand nom de réseaux, y compris des réseaux centralisés comme *Twitter*.

Ayant découvert les réseaux fédérés avec *Diaspora*, puis ayant suivi la tendance sur *Mastadon*, je me suis senti chez moi quand j'ai pu avoir à la fois mes contacts de ces deux réseaux, plus mon fil *Twitter*.

* [mémo à trier](/logiciels-libres/friendica/notes)
