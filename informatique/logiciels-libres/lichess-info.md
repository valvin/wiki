---
title: Lichess Info
description: bot permettant d'animer une équipe sur Lichess
published: true
date: 2022-11-20T17:36:18.232Z
tags: libre, développement, python
editor: markdown
dateCreated: 2022-11-20T17:36:18.232Z
---

# Description

Lichess-info était initialement prévue pour être un simple exporter Prometheus mais est finalement devenu une interface web pour une équipe Lichess et également un *bot* pour notifier des parties des membres de l'équipes.

* [Source](https://gitlab.com/valvin/lichess-info)
* [Démo](https://li.valvin.fr)