---
title: Reveal Hugo - slides en markdown
description: intégration de revealJS dans hugo
published: true
date: 2022-11-25T21:53:48.222Z
tags: hugo, markdown, revealjs, présentation, outils
editor: markdown
dateCreated: 2022-11-25T21:53:48.222Z
---

# Présentation au format markdown
Il s'agit d'un thème Hugo écrit par dzello pour créer des présentation Reveal.js.

* [reveal-hugo](https://reveal-hugo.dzello.com/#/)
* [reveal.js](https://revealjs.com/)
* [hugo](https://gohugo.io/)

Je l'utilise pour générer mes présentations et les stocker *as code* sur un dépôt git. Reveal.js est initialement orienté HTML mais supporte markdown. Cependant le markdown est mélangé dans une seule page html ce qui est un peu perturbant à mon gout. L'intégration dans hugo facilite la gestion du contenu et permet de mieux le versionner dans un dépôt git.

## Créer une présentation *from scratch*

```bash
hugo new site <nom_de_la_prez>
cd <nom_de_la_prez>
git init
git submodule add git@github.com:dzello/reveal-hugo.git themes/reveal-hugo
vim config.toml
```
ajouter/modifier:

```toml
theme = "reveal-hugo"

[markup.goldmark.renderer]
unsafe = true

[outputFormats.Reveal]
baseName = "index"
mediaType = "text/html"
isHTML = true
```
et dans `content/_index.md`:

```markdown
+++
title = "Ma présentation"
outputs = ["Reveal"]
+++

# Hello world!

Allez hop, je fais du reveal-hugo!
```

pour lancer le site:

```bash
hugo serve
```
