---
title: sed
description: outils de manipulation de chaines de caractères
published: true
date: 2022-07-25T06:36:41.877Z
tags: sed
editor: markdown
dateCreated: 2022-07-25T06:36:41.877Z
---

```bash
# pour retirer les retours chariots d'un fichier
sed ':a;N;$! ba;s/\n//g' <le_chemin_de_mon_fichier>
```