---
title: Projets
description: Projets Libre / Opensource auxquels je participe / contribue / développe
published: true
date: 2022-11-20T17:29:25.886Z
tags: libre, opensource, projet, contributions
editor: markdown
dateCreated: 2020-04-11T13:25:40.621Z
---

* [Pepper&Carrot *une bande-dessinée entièrement Libre*](peppercarrot)
* [Lichess-Info *un bot / leaderboard pour les équipes Lichess*](lichess-info)
{.links-list}