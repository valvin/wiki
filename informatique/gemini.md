---
title: Gemini
description: un protocole volontairement très limité pour diffuser des contenus texte
published: true
date: 2022-11-24T21:31:58.444Z
tags: gemini, smallweb
editor: markdown
dateCreated: 2022-11-21T19:56:47.682Z
---

# Le projet Gemini
Gemini est un projet définissant un protocole volontairement très limité et restreint pour exposer du contenu principalement texte. L'idée est que celui-ci soit implémentable le plus simplement possible et puisse fonctionner sur tout type de matériel. Il est limité pour qu'on ne puisse pas l'utiliser pour en faire autre chose comme ce qui est arrivé avec HTML et javacript sur le "web" que l'on connait aujourd'hui. En plus du protocole, une syntaxe est définie pour écrire du contenu. Il s'agit du *gemtext*.

Pour accéder aux capsules *gemini*, il est nécessaire d'avoir un navigateur prenant en charge ce nouveau protocol. Cela ne fonctionne pas sur Firefox, Chrome...

* [Site officiel du projet Gemini](https://gemini.circumlunar.space/)
* [Langrange, navigateur gemini(et+)](https://codeberg.org/skyjake/lagrange)

## Mes capsules

Pour ma part, j'utilise l'implémentation du serveur *Agate* et le générateur de site *kiln*.

* [Ma capsule principale (fr)](gemini://gmi.valvin.fr) (gemini) / [Version https (proxy)](https://gmi.valvin.fr)
* [Ma capsule sur tilde.club (en)](gemni://tilde.club/~valvin) / [Version https (proxy](https://tilde.club/~valvin/)
* [Image docker agate + kiln + synchronisation git](https://gitlab.com/valvin/gemini-agate-image)

## Kiln

* [Lien du dépôt du projet](https://git.sr.ht/~adnano/kiln/)
* [Lien de la doc](https://git.sr.ht/~adnano/kiln/tree/master/item/docs/kiln.1.scd) (source du man)
* [Lien site projet](https://kiln.adnano.co/)



