---
title: systemd - commandes systemctl et journalctl
description: commandes journalctl et systemctl
published: true
date: 2020-12-15T14:00:00.000Z
tags: linux, commande, systemd, systemctl, journalctl
---

`systemd` permet de faire bien des choses mais une de ses fonctions premières et de pouvoir piloter les différents services (*unit*) du système.

## Services

Les services sont manipulés par la commande `sysemctl`
opérations basiques:

```bash
# démarrer un service
systemctl start <nom_du_service> [<autre_service>]
# arrêter un service
systemctl stop <nom_du_service> [<autre_service>]
# voir l'état d'un service
systemctl status <nom_du_service> [<autre_service>]
```

activation / désactivation de services:

```
# activation du service au démarrage
systemctl enable <nom_du_service>
# même chose avec un démarrage immédiat
systemctl enable --now <nom_du_service>

# désactivation d'un service
system disable <nom_du_service>
```

## Jounal

Pour lire les journaux de `systemd` on utilise la commande `journalctl`

```bash
# affiche tous les journaux du plus récents au plus anciens
journalctl
# affiche les journaux du service précisé
journalctl -xu <nom_du_service>
```

Les journaux peuvent vite prendre beaucoup de place, il est possible de les purger:

```
# efface les journaux plus anciens que 2 jours
sudo journalctl --vacuum-time=2d
```

