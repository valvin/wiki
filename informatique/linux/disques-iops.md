---
title: Performances disques IOPS
description: commandes utiles pour vérifier les performances de ses disques sous linux
published: true
date: 2020-06-11T19:48:20.332Z
tags: linux, iops, dd, fio, iostat
---

Ce n'est pas un sujet qui me passionne mais force est de constater que connaitre les performances disques de son serveur est un important.

La raison est que lors d'une saturation des disques sur le serveur, les process sont en attentes tant que l'opération n'est pas terminée. Il n'est possible d'executer qu'autant de process que de coeurs en même temps. De ce fait dans cette situation le *load* du serveur augmente très vite car beaucoup de process se mettent en fil d'attente. Le temps d'attente lié aux disques s'appelle __*iowait*__.


Les performances des disques se mesurent distinctement en lecture et en écriture avec les indicateurs suivants:
* IOPS : le nombre d'actions que l'on peut faire sur le disque en une seconde
* Bande-passante : la quantité de données que l'on peut transférer en une seconde
* La latence : il s'agit du temps de réponse du disque

> TODO : il y aussi le cache qui a un rôle important
{.is-warning}

## iostat

Cette commande permet d'afficher les informations des différents disques du système. Il est difficile d'interpréter les résultats de cette commande en une seule éxécution, c'est pour cela qu'il est préférable de la lancer à intervalle régulier.

```bash
# affiche les données étendues toutes les secondes
iostat -x 1

# Exemple de saturation en lecture :

avg-cpu:  %user   %nice %system %iowait  %steal   %idle
           3.94    0.00   16.46    9.48    0.00   70.13

Device            r/s     w/s     rkB/s     wkB/s   rrqm/s   wrqm/s  %rrqm  %wrqm r_await w_await aqu-sz rareq-sz wareq-sz  svctm  %util
vda           12250.00    0.00  48996.00      0.00     0.00     0.00   0.00   0.00    0.42    0.00   4.76     4.00     0.00   0.08 100.00

# Exemple de saturation en écriture:

avg-cpu:  %user   %nice %system %iowait  %steal   %idle
           2.27    0.00   21.33   15.73    0.00   60.66

Device            r/s     w/s     rkB/s     wkB/s   rrqm/s   wrqm/s  %rrqm  %wrqm r_await w_await aqu-sz rareq-sz wareq-sz  svctm  %util
vda              0.00  612.00      0.00 250736.00     0.00    21.00   0.00   3.32    0.00  160.59  93.89     0.00   409.70   1.63 100.00
```
Les informations obtenues :
* `%iowait` : il s'agit du pourcentage de temps attendu par le processeur lié uniquement aux opérations sur le disque (lecture ou écriture) ; ci-dessus `15.73%` signifie que sur 1s, le processeur est bloqué 157ms pour une opération disque.
* `%util` : correspond à l'utilisation de la capacité de notre disque; ici on utilise 100% donc la totalité.
* `r_await` ou `w_await` : correspond au temps de réponse du disque pour traiter l'opération. ici en lecture, 0.42ms ; en écriture 160.59ms ce qui est élevé.
* `r/s` ou `w/s` : le nombre d'opération par seconde en lecture ou écriture. ici 12.2k IOPS en lecture, 612 IOPS en lecture.
* `rkB/s ou wkB/s` : quantité de donnée transférée en lecture ou écriture. 47.8MB/s en lecture et 244.86MB/s en écriture.

> Attention ceux-sont des données instantannées, il faut donc pas les traiter unitairement.
{.is-warning}

## fio

```bash
# effectue un test en lecture sur un fichier de 4Go avec des blocs de 4k
fio --randrepeat=1 --ioengine=libaio --direct=1 --gtod_reduce=1 --name=test --filename=random_read.fio --bs=4k --iodepth=64 --size=4G --readwrite=randread
# effectue un test en écriture sur un fichier de 4Go avec de blocs de 4k
fio --randrepeat=1 --ioengine=libaio --direct=1 --gtod_reduce=1 --name=test --filename=random_write.fio --bs=4k --iodepth=64 --size=4G --readwrite=randwrite
# effectue un test lecture/écriture avec un ration 75%/25%
fio --randrepeat=1 --ioengine=libaio --direct=1 --gtod_reduce=1 --name=test --filename=random_read_write.fio --bs=4k --iodepth=64 --size=4G --readwrite=randrw --rwmixread=75
```

[source - unixmen.com](https://www.unixmen.com/how-to-measure-disk-performance-with-fio-and-ioping/)

## hdparm

```bash
# donne la bande-passante en lecture (sans cache)
hdparm -t /dev/vda
```
## dd

```bash
# écrit un fichier de 10000 * 4ko sur un seul thread et attend la confirmation de l'écriture sur le dsique.
dd if=/dev/zero of=file.img bs=4096 count=10000 oflag=dsync
# la même chose en lecture
dd if=file.img of=/dev/null bs=4096
```
