---
title: Coriandre ou persil chinois
description: La coriandre, herbe aromatique qui s'utilise en feuille ou en graine
published: true
date: 2022-09-04T18:35:03.752Z
tags: jardinage, aromatique
editor: markdown
dateCreated: 2022-09-04T18:35:03.752Z
---

La coriandre est utilisée pour aromatisée nos plats. On peut utiliser ses feuilles un peu comme le persil. C'est pour ça qu'on l'appel persil chinois.

Pour l'utiliser en feuilles, il faut être prudent car avec la chaleur, elle monte vite en graine. Il faut donc bien l'aroser mais malgré tout en début d'été c'est compliqué.

Pour l'utiliser en graines, il faut attendre un peu plus longtemps. La plante monte en graine et commence à sécher / brûnir. C'est à ce moment que je récolte. Les graines peuvent être utiliser en fin aout en semis. Je trouve plus facile d'avoir des feuilles en septembre/octobre.