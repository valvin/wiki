---
title: Weechat - client IRC
description: client IRC pour terminal
published: true
date: 2022-09-10
tags: irc,weechat
editor: markdown
---

Weechat permet de se connecter aux serveurs irc comme libera.

Voici quelques commandes pour aider la configuration:

```bash
# ajout du serveur libera sur l'adreese ssl irc.libera.chat port 6697
/server add libera irc.libera.chat/6697 -ssl

# définition des pseudos à utiliser
/set irc.server.libera.nicks "pseudo_principal,_pseudo_principal"

# pour se connecter à l'ouverture de weechat
/set irc.server.libera.autoconnect on

# pour la sauvegarde des identifiant (vérifier si c'est le meilleur moyen)
/set irc.server.libera.sasl_username "pseudo"
/set irc.server.libera.sasl_password "xxxxxxx"

```

[source](https://weechat.org/files/doc/stable/weechat_quickstart.fr.html)
