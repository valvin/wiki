---
title: Électronique
description: Exemples de montages, explication de composant
published: true
date: 2020-05-01T16:25:01.701Z
tags: electronique, composant, montage
---

- [Décodeur BCD - 7 segments](/electronique/decodeur-bcd-7-segements)
- [Montage double afficheurs 7 segments](/electronique/montage-double-afficheur)
- [Notes rapides](/electronique/notes)
{.links-list}