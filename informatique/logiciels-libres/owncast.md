---
title: Owncast
description: outils permettant de diffuser des "streams" à partir d'outils comme OBS
published: true
date: 2022-11-20T19:19:46.857Z
tags: libre, activitypub, auto-hébergement, diffusion, stream, fediverse
editor: markdown
dateCreated: 2022-11-20T15:22:20.345Z
---

## Liens utiles

* [Site officiel](https://owncast.online/)
* [Mon instance](https://live.valvin.fr)
{.links-list}

Owncast permet de diffuser facilement à partir d'un logiciel tel qu'OBS. Il suffit de paramétrer le serveur RTSP et sa clé pour lancer le *stream*. 

Fonctionnalités:

* système de commentaires directement sur la page du stream
* fédéré avec activitypub: `@valvin@live.valvin.fr`