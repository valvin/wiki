---
title: Git - commandes
description: liste des commandes git que j'utilise
published: true
date: 2021-03-17T20:38:41.085Z
tags: commande, git
editor: markdown
dateCreated: 2020-05-28T20:27:37.760Z
---

# Commandes Git

## Les commandes basiques

```bash
# récupération d'un dépot
git clone <url_repo>
# récupération des modifications
git pull [<nom_du_remote> <nom_de_la_branche>]
# commit des changements de tous les fichiers suivis
git commit -am "<commentaire>"
# commit des changements d'un fichier spécifique
git commit <nom_du_fichier> -m "<commentaire>"
# ajout d'un fichier à suivre dans git (ou ajout au prochain commit si le fichier est déjà suivi)
git add <fichier_ou_pattern>
# envoie des changements commités
git push [<nom_du_remote> <nom_de_la_branche>]
# changer de branche
git checkout <nom_de_la_branche>
# créer une nouvelle branche à partir de celle ou l'on est
git checkout -b <nom_de_la_branche>
# obtenir les informations du dépot distant
git fetch
# savoir dans quel statut se trouve notre dépot local
git status
# visualiser les changements en cours
git diff
# afficher le log des commits
git log 
```

## Les commandes fréquentes

```bash
# retirer un fichier du prochain commit (modification conservées)
git reset <nom_du_fichier>
# réinitialiser un fichier (supprime les modifications)
git checkout <nom_du_fichier>
# remettre le dépôt au dernier commit du dépot local (supprime les modifications)
git reset --hard HEAD
# modifier le commentaire du dernier commit
git commit --amend
# modifier le dernier commit avec les changements réalisés (nécéssite de forcer la mise à jour)
git add . && git commit --amend --no-edit
# pour appliquer les changements d'une autre branche dans la branche locale
git merge <nom_de_la_branche_locale>
# afficher les branches distantes
git branch -r
# affiche les dépots distants
git remote -v
# afficher le log sur une ligne dans stdout
git log --oneline|cat -
# créer une branche à partir d'un tag
git checkout -b <nom_de_la_branche> <nom_du_tag>
```

## Les commandes de ré-écriture du journal

La manipulation du journal de *commit*, à utiliser avec précautions car elles ré-écrivent l'historique et nécessitent un forçage pour mettre le dépôt distant à jour.

Lors d'un rebase en mode intéractif, il est possible d'annuler celui-ci dans `vim` en faisant `:cq`

```bash
# ré-écrire ses commits après ceux d'une branche (peut générer des conflits)
# utilise la branche du dépôt local
git rebase <nom_de_la_branche>
# ou sinon il faut spécifier le dépôt distant
git rebase <nom_du_remote> <nom_de_la_branche>
# ré-écrire les commits en mode interactif
git rebase -i <nom_du_commit_precedent_non_modifié>
# ou à partir du dernier commit
# <num> étant le nombre de commit en dessous de HEAD
git rebase HEAD~<num>
# quand on ré-écrit l'histoire, il faut forcer ces changements
git push --force
# quand une autre personne à fait un forçage, il faut forcer la récupération
# attention à la perte des commits non présents sur dépôt distant
git pull --force
```

## Les commandes pour mettre de côtés ses changements
lorsque l'on fait un rebase, il faut que le statut du dépôt soit sans aucune modification. Il faut soit *commiter* ces changements, soit les mettres de côté avec `stash` :

```bash
# pour mettre de côté toutes les modifications des fichiers suivis
git stash
# pour visualiser toutes les modifications mises de côté
git stash list
# pour les ré-appliquer à partir de leur numéro
git stash apply <num>
```