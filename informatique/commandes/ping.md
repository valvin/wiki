---
title: Linux - ping
description: commande ping permet d'envoyer des paquets à une hôte distant
published: true
date: 2020-02-07T00:00:00.000Z
tags: linux, commande, ping, réseau
editor: markdown
---

## ping

Certainement le cas le plus fréquents `ping <adresse_ip_ou_nom_d_hote>`. Permet d'obtenir un temps de réponse de l'hôte. Si on souhaite faire un diagnostique, on regarde :

* le numéro de séquence `icmp_seq`, en cas de problème, il y a des trous ou plus rarement arrive dans le désordre
* le pourcentage de paquets perdus, généralement si on passe au dessus des 3% ce n'est pas bon signe
* et bien entendu le temps d'aller-retour du paquet minimum/moyen/maximum qui doivent avoir un déviation la moins élevée.

Il est possible de désactiver le mode continue en précisant un nombre de paquet avec l'option `-c`.

```bash
# vérification de la réponse d'un hôte distant en continue
$ ping 192.168.1.1
PING 192.168.1.1 (192.168.1.1) 56(84) octets de données.
64 octets de 192.168.1.1 : icmp_seq=1 ttl=64 temps=6.51 ms
64 octets de 192.168.1.1 : icmp_seq=2 ttl=64 temps=5.37 ms
64 octets de 192.168.1.1 : icmp_seq=3 ttl=64 temps=4.42 ms
64 octets de 192.168.1.1 : icmp_seq=4 ttl=64 temps=2.73 ms
...
--- statistiques ping 192.168.1.1 ---
4 paquets transmis, 4 reçus, 0% packet loss, time 3003ms
rtt min/avg/max/mdev = 2.731/4.757/6.507/1.383 ms
```

Autre usage intéressant, que j'utilise pour les diagnostique wifi pour valider un bon roaming mais peut être pour d'autres cas d'usage. Il s'agit d'une tempête de ping, option `-f` (*flood*), à intervalle court. On ne reçoit plus de métriques mais seulement des `.`. Un point s'affiche à l'envoie d'un paquet et on en retire un à la réception du retour. De ce fait, si on a des points qui s'affichent, cela signifie qu'on perd des paquets. L'intervalle minimum entre deux paquets est de 200ms que l'on peut définir avec l'option `-i`. L'affichage `E` correspond à une erreur sur l'hôte, par exemple en désactivant le wifi. `ping -f -i0.2 <adresse_ip_ou_nom_d_hote>`

```
ping -f -i0.2 192.168.1.1  
PING 192.168.1.1 (192.168.1.1) 56(84) octets de données.
..EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE^C
--- statistiques ping 192.168.1.1 ---
681 paquets transmis, 607 reçus, 10.8664% packet loss, time 137021ms
rtt min/avg/max/mdev = 2.071/12.524/41.932/6.794 ms, ipg/ewma 201.501/15.769 ms
```
