---
title: Notes rapides
description: quelques notes pendant que je bricole sur l'électronique
published: true
date: 2020-04-18T09:03:13.017Z
tags: electronique, notes
---

## Référence CI logiques

| Référence   | Fonction | Documentation|
| ---         | ---   | ---           |
| 74HC595N    | Registre à décalage 8 bits| [datasheet](https://pdf1.alldatasheet.com/datasheet-pdf/view/527966/NXP/74HC595N.html)|
| 74HC164N     | Registre à décalage 8 bits | [datasheet](https://pdf1.alldatasheet.com/datasheet-pdf/view/101613/PHILIPS/74HC164N.html) |
| 74LS374N    | Registre 8 bits flip-flop| [datasheet](https://pdf1.alldatasheet.com/datasheet-pdf/view/64054/HITACHI/74LS374.html)|
| HCF4511BE   | Convertisseur BCP - 7 segments | |
| CD74HCF151E | Multiplexeur 8 bits | [datasheet](https://www.alldatasheet.com/view.jsp?Searchword=D74HCT151E&sField=3)|
|TL074CN       | Amplificateur opérationnel | [datasheet](https://pdf1.alldatasheet.com/datasheet-pdf/view/5777/MOTOROLA/TL074CN.html) |
| LM339N       | 4 Comparateurs - détecteurs de seuil | [datasheet](https://pdf1.alldatasheet.com/datasheet-pdf/view/3071/MOTOROLA/LM339N.html) |
| HEF4051BP    | Multiplexeur / démultiplexeur 8 canaux analogiques | [datasheet](https://pdf1.alldatasheet.com/datasheet-pdf/view/17717/PHILIPS/HEF4051B.html) |
| HCF4053BE    | Multiplexeur / démultiplexeur 3 canaux analogiques | [datasheet](https://pdf1.alldatasheet.com/datasheet-pdf/view/22343/STMICROELECTRONICS/HCF4053BEY.html) |
| HCF4011BE    | 4 portes NAND | [datasheet](https://pdf1.alldatasheet.com/datasheet-pdf/view/243167/STMICROELECTRONICS/HCF4011BE.html) |
| NE555        | Oscillateurs  | [datasheet](https://pdf1.alldatasheet.com/datasheet-pdf/view/23384/STMICROELECTRONICS/NE555.html) |
| PT2262/2272  | Modulateur / démodulateur pour IR / RF| [datasheet 2262](https://pdf1.alldatasheet.com/datasheet-pdf/view/391539/PTC/PT2262.html) / [datasheet 2272](https://pdf1.alldatasheet.com/datasheet-pdf/view/35128/PTC/PT2272.html) |

## Régulateurs de tensions

| Référence   | Fonction | Documentation|
| ---         | ---   | ---           |
| 7812CT         | Régulateur 12V   | [datasheet](https://pdf1.alldatasheet.com/datasheet-pdf/view/831260/NJRC/7812A.html)           |

## Autres
| Référence   | Fonction | Documentation|
| ---         | ---   | ---           |
| TDA2006 | Ampli audio 12W | [datasheet](https://pdf1.alldatasheet.com/datasheet-pdf/view/25038/STMICROELECTRONICS/TDA2006.html)|
| KA2201  | Ampli audio 1.2W | [datasheet](https://pdf1.alldatasheet.com/datasheet-pdf/view/37140/SAMSUNG/KA2201.html) |
| S22MD3 | 2 optocoupleurs | [datasheet](https://pdf1.alldatasheet.com/datasheet-pdf/view/43503/SHARP/S22MD3.html) |

