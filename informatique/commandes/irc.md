---
title: IRC
description: quelques commandes IRC à retenir
published: true
date: 2022-09-10T17:34:31.543Z
tags: commandes,irc
---

## Pour le pseudo / authentification

```bash
# Pour changer de pseudo
/nick <nouveau_pseudo>
# Bien souvent il faut s'authentifier pour utiliser son pseudo
# Pour cela, on envoie un message privé à l'utilisateur NickServ 
/msg NickServ IDENTIFY <password>
# ou avec le nickname 
/msg NickServ IDENTIFY <nickname> <password>

# Pour avoir plus de commande
/msg NickServe HELP
```

## Pour les canaux

```
# rejoindre un canal <nom_du_canal>
/join #<nom_du_canal>

# lister tous les canaux du serveur
/list

# rechercher un canal
/list <mot_clé>

```

