---
title: MySQL - requêtes utiles
description: mémo de quelques informations / requêtes utiles pour MySQL
published: true
date: 2021-03-14T19:20:15.515Z
tags: bdd, mysql, requête
editor: markdown
dateCreated: 2020-08-27T13:34:32.036Z
---

> en cours
{{ .is-warning }}

## Requête

```sql
#affiche la liste des tables
SHOW TABLES;
# affiche les colonnes d'une table
DESCRIBE <nom_de_la_table>;
SHOW COLUMNS FROM <nom_de_la_table>;

# affiche les index d'une table
SHOW INDEXES from <nom_de_la_table>
```

Pour les tables avec des caractères spéciaux comme `-`, il est nécessaire d'échapper la table avec des `` ` `` ; exemple : ``select * from `my-table`;``