---
title: Jardinage
description: Mes tentatives pour la réalisation d'un potager
published: true
date: 2021-09-09T19:10:08.844Z
tags: jardinage, potager, légumes
editor: markdown
dateCreated: 2020-04-12T12:35:01.856Z
---

# 2021

## Aout

* Poireaux: repiquage un peu avant le 15/08.
* Épinards: semis entre les poireaux
* Carottes: récolte au fil de l'eau

## Avril - Juillet

Mauvais suivi ici, voici grosse maille ce qui s'est passé dans le jardin sur cette période

* Salades : avec les tunnels cela a bien fonctionné et les limaces n'ont pas attaqué. Par contre tout est arrivé en même temps et on a pas réussi à tout manger.
* Concombres (Markor): 
  * après les avoir sortis même à l'arbri tous les plan de concombre ont pris froid. ils sont plus fragiles que les courgettes.
  * nouveau semis: sur 3 plans, 1 a fait des concombres tout blanc
  * premier concombres ramasser mi-juillet
  * test de faire grimper les concombres sur un tipi mais cela n'a pas fonctionné
* Courgettes:
  * quelques plans ont pris froid mais globalement ont tenu le choc. Cependant, cette année les plans *végette* et les courgettes ne sont pas très grosses. A voir si le souci de végétation est lié au froid lorsqu'ils étaient en plan ou au mauvais temps que l'on a depuis le mois juin.
 * Tomates:
   * en tout 6 pieds différents mais malheureusement bien qu'ils avaient très bien commencé, le temps a été très humide sur le mois de  Juin et Juillet. Ils ont tous attrapé le midiou. Même les plans "sauvages" de tomates cerises ont attrapé la maladie.
* Haricots:
  * Premier semis n'a pas fonctionné : trop froid
  * Deuxième semis fin juillet: les première graines ont germé
* Carottes:
  * Cela a bien fonctionné, on peut commencer à en récolter fin Juillet. (semé en avril)
* Pommes de terre:
  * Variété charlotte: tout a bien fonctionné et bonne récolte mi-juillet.
* Échalottes:
  * Récoltées mi-juillet. L'échalottes n'aiment pas les sols trop humides, de ce fait il y en avait plusieurs qui ont pourries.
* Radis:
  * Bonne récolte mais attention, il faut bien arroser.

## Mars

* Ajout de compost + bêchage (20)
* Semis salades (feuilles de chêne et batavia) en intérieur (27)
* Semis courgettes et concombre en intérieur (27)
* Semis radis en extérieur (27)

### Note
Semis salade, courgette et concombre fait en lune descendente ce qui n'est pas l'idéal. OK pour les radis.
Les poireaux repiqués à l'automne reste petit mais vont bientôt pouvoir être ramassé.

---
# 2020

## Potager

### Semis

#### > Intérieur

* Salades : 
  * ~~Batavia & Jardinier semés le 05/04 dans boite d'oeurf intérieur~~
    * 12/04 : 1^ère^ germination 
    * 19/04 : les germes ne se développe pas
    * 22/04 : pas d'évolution, rebus
  * ~~Batavia dans godet (terre + terreau + café) le 19/04~~
    * 25/04 : pas de germination visible
    * 01/05 : toujours rien :(
    * 08/05: toujours rien, mauvaises graines
* Courgette Jaunes et Lola sémées le 26/03 en godet / intérieur 
  * 03/04 germination
  * 19/04 rempotage 
  * 26/04 mis en terre de 2 plants sur 4 ; reste 2 plants
  
* ~~Oeillets d'inde (graines récoltés) en boite d'oeur intérieur le 11/04~~
  * 19/04: toujours pas levé
  * 22/04 : pas d'évolution, rebus
* Persil semés le 12/04 (une nuit de trempage) en godet (terreau + terre + marc)
  * 19/04 : il semble qu'une première germination apparait (à confirmer)
  * 25/04: des germes se développent mais difficile de confirmer qu'il s'agit bien de persil (mauvaise herbe)
* Basilic grandes feuilles semés le 12/04 en godet (terreau + terre + marc)
  * 17/04 : premières germinations
  * 25/04: plusieurs godets se développent bien
  * 08/05: seulement 2 godets sont bien remplis. le développement stagne.
* Butternut semés le 19/04 en godet (terreau + terre + marc)
  * 25/04 : pas de germe visible
  * 01/05 : toujours rien
  * 08/05: toujours rien
* Tomates cerises semés le 19/04 en godet (terreau + terre + marc)
  * 25/04: pas de germe visible
  * 28/04: germination et début de développement un dans chaque pot
  * 08/05: ça pousse doucement, plusieurs germes par pot.
  
#### > Potager
* Carrotes nantaise et ??? semés le 11/04 en pleine terre + voile
  * 24/04: premières germinations visibles
  * 01/05: bonne germination (toujours sous voile)
  * 08/05: ça pousse doucement mais surement (toujours sous voile)
* Haricots verts semés le 11/04 en pleine terre + voile
  * 20/04: première germination
  * 24/04: développement de plusieurs graines 
  * 01/05: la totalités "godets" ont germés et les plants se développent (toujours sous voile)
  * 07/05: les limaces (ou escargots) s'en sont pris à quelques pieds
* Poireaux semés le 11/04 en pleine terre - enplacement très ensoleillé
  * 24/04: pas de germe visible
  * 28/04: premier germes visibles
* Oeillets d'inde (graines récoltés) le 11/04 - emplacement très ensoleillé
  * 24/04: pas de germe visible
  * 28/04: peut être quelques germes mais pas évident vu leur taille
* Salade :
  * 23/04: repiquage de deux plants de salade achetés
  * 01/05: pas de limace, les plants se développent bien
* Courgettes:
  * 26/04: repiquage de deux plants issus du semis
  * 30/05 : suite à averse de grêle, la plus grande feuilles des deux plants cassés
  * 06/05 : coup de froid, elles sont encore debout mais on pris un sérieux coup.
* Concombres:
  * 26/04: repiquage d'un plan acheté le 23/04
  * 06/05: coup de froid, il n'a pas survécu... penser à protégrer la prochaine fois

#### Jardinière enfant
* Persil : repiquage le 23/04 plant acheté
* Salade : 
  * requipage plant acheté le 23/04
    * le plant se développe bien
  * ~~semis batavia mélangé avec radis le 23/04~~
    * 01/05: pas de germes visibles
    * 08/05: toujours rien, les graines ne sont plus bonnes
* Radis : semis mélangé avec salade le 23/04
  * 28/04: premières germinations
  * 08/05: ça pousse bien, éclaircissage là où c'est trop dense.

### Terrain

* retrait framboise + barrière
* labour tardif le 03/04
* houe lorraine le 11/04

## Jardin

* Boutourage lavande le 11/04 + tentative modelage fil de fer
  * 19/04 : Les boutures avec fils de fers ne semble pas avoir prises. Les autres semblent ok.
  * 25/04 : les deux plans sans fils de fer semble en bonne forme. Les autres ont un peu séchés.
  