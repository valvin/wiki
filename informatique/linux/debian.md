---
title: Debian
description: 
published: true
date: 2022-03-02T12:13:47.452Z
tags: debian, linux
editor: markdown
dateCreated: 2022-03-02T12:13:47.452Z
---

# Debian

Petites infos autour de debian

## Mise à jour

```bash
# version courante
cat /etc/debian_version
# mise à jour avant mise à niveau
apt update && apt upgrade && apt full-upgrade
# modification des dépôts (ici buster 10 => bulleye 11)
sed -i 's/buster/bullseye/g' /etc/apt/sources.list
# mise à niveau des dépôts
apt update
# première mise à niveau des paquets existants
apt upgrade --without-new-pkgs
# mise à niveau complète
apt full-upgrade
# redémarrage
init 6