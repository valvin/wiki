---
title: Décodeur BCD / afficheur 7 ségments
description: Les composants comme le HCF4511BE permet de décoder un chiffre binaire en code 7 segments
published: true
date: 2020-04-13T06:18:16.580Z
tags: electronique, bcd, 7-segments, afficheur, composant
---

Le composant `HCF4511BEY` permet de décoder un code binaire *BCD*  en un code pour un afficheur 7 ségments.

Les spécifcations du composant sont disponibles [ici](https://pdf1.alldatasheet.com/datasheet-pdf/view/206834/STMICROELECTRONICS/HCF4511BEY.html)

Le fonctionnement est le suivant :

![hcf4511be-functional-diagram.png](/electronique/hcf4511be-functional-diagram.png)

On envoie donc un code binaire sur 4 bits sur les *BCD inpunts*. Cette entrée doit correspondre à un chiffre entre `0` et `9`. Celui-ci sera reproductible sur un afficheur 7-segments avec les *7-segment outputs*.

Il également 3 autres *pins* qui permettent :

* Latch Enable / Strobe Input (pin 5) : si à `0` l'entrée est envoyée au décodeur, sinon elle est vérouillée.
* Blank input (pin 4)  : s'il est à `0` peu importe l'entrée, l'afficheur est éteint
* Lamp test input ( pin 3) : s'il à `0` peu importe l'entrée, il affichera un `8`

La table dé vérité est donc la suivante:

![hcf4511be-truth-table.png](/electronique/hcf4511be-truth-table.png)

Test du circuit :

![hcf4511be_test.jpg](/electronique/hcf4511be_test.jpg)
