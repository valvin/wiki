---
title: Prometheus
description: note à moi même sur l'usage de prometheus
published: true
date: 2020-07-03T11:16:54.601Z
tags: métriques, prometheus, collectd
editor: markdown
---

## Qu'est ce que c'est?


[Prometheus](https://prometheus.io) est un outils permettant de collecter des *metrics* en requêtant différents *exporter*. Ces *metrics* peuvent être de tout types / origines.

Ces données sont stockées dans une base temporelle et il est alors possible de les requêter pour définir des alertes (avec Alertmanager par exemple), ou réaliser des tableaux de bords (avec Grafana).

De nombreux *exporters* sont maintenus par les développeurs de *prometheus* ([liste](https://prometheus.io/docs/instrumenting/exporters/)) mais il est également possible d'en développer facilement avec les différents SDK à disposition.

Voici ceux que j'utilise : 

* collectd, le plugin pour les version récentes, collectd_exporter pour les autres.
* blackbox
* jenkins_exporter
* nexus_exporter


## Requêter *Prometheus* avec PromQL


Le langage *PromQL* a ses particulartiés, notamment lors que l'on souhaite faire des calculs avec différentes métriques.

Il y a le nom de la métrique, les étiquettes qui lui sont associées et bien entendu sa valeur.

> WIP
{.is-warning}

Pour faire des calculs sur des métriques, il faut ingorer les labels qui ne sont pas identiques avec la fonction `ignoring(<label)` que l'on met juste après l'opérateur.

- Requête pour échantilloner une donnée de type `count`, ici les *iops* en lecture remontés par *collectd* :

```
rate(collectd_disk_disk_ops_read_total{instance="<server_name>"}[5m])
```

- Requête pour calculer l'espace disponible de métriques remontées par collectd : 

```
  collectd_df_df_complex{df="root", type="free"} /  ignoring(type) (collectd_df_df_complex{df="root", type="free"} + ignoring(type) collectd_df_df_complex{df="root", type="used"} ) 
```


## Alertes

Pour définir une alerte, il faut ajouter un fichier dans la liste `rule_files` du fichier `prometheus.yml` :

```yaml
global:
  scrape_interval:     60s

scrape_configs:
  - job_name: '<job_name>'
    static_configs:
      - labels:
          source: collectd 
          server_group: <server_group>
        targets: 
          - '<server_name>:9103'
rule_files:
  - <my_alert_rule>.yml
```

et dans le fichier `my_alert_rule.yml`, si on prend pour exemple l'espace libre remontée par *collectd* :

```yaml
groups:
  - name: Storage free space
    rules:
    - record: "server:df:root:free_space"
      expr: (collectd_df_df_complex{df="root", type="free"} /  ignoring(type) (collectd_df_df_complex{df="root", type="free"} + ignoring(type) collectd_df_df_complex{df="root", type="used"} )) * 100
    - alert: Low free space
      expr: server:df:root:free_space < 10
      for: 5m
      labels:
        severity: major
      annotations:
        summary: "Instance {{ $labels.instance }} low free space"
        description: "{{ $labels.instance }} has only {{ $value }} % free space"
  - name: High Load
    rules:
    - alert: High load
      expr: load5:collectd_load_midterm > 7
      for: 5m
      labels:
        severity: major
      annotations:
        summary: "Instance {{ $labels.instance }} hight CPU usage"
        description: "{{ $labels.instance }} has a load on 5min {{ $value }}"

```
