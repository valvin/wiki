---
title: PostgreSQL - info utiles
description: quelques requêtes et autres infos pour PostgreSQL
published: true
date: 2022-08-29T13:28:56.023Z
tags: bdd, postgresql
editor: markdown
dateCreated: 2020-06-03T15:48:31.502Z
---

## Maintenance de la base
### statistiques

Les statistiques sont des informations qu'utilisent l'optimiseur de requête afin d'identifier le meilleur schéma d'éxecution d'une requête. Elles sont donc très importantes pour les performances.

Pour connaitre le dernier calcul de stats sur une table, on peut utiliser la requête:
```sql
select relname, last_analyze from pg_stat_user_tables;
```

Pour lancer le caclul de stats sur une base de données complète :

```sql
analyze;
```

Il est également possible de lancer le calcul de stats en combinaison avec vacuum.

### vacuum

Le *vacuum* est le fait de libérer l'espace disque disponibles.
Il existe trois type de *vacuum* :
* *vacuum* : marque les enregistrements obsolètes réutilisables
* *vacuum full* : bouche les trous, un peu comme un *defrag* (attention, lock les tables)
* *vacuum full freeze* : visiblement à ne pas faire :)

Une fois connecté à la base :

```sql
select relname, last_vacuum from pg_stat_user_tables;
```

Il est courant de lancer vacuum en combinaison avec le calcul de stats.

```sql
 vacuum analyze;
```

Pour le lancer sur toutes les bases :

```bash
#!/bin/bash
# vacuum_analyze.sh
SCRIPT=`readlink -f $0`
SCRIPTPATH=`dirname $SCRIPT`
echo "START TIME - "$(date)
for bdd in `psql -At -c "select datname from pg_database where datistemplate=false and datname!='postgres' and pg_is_in_recovery()=false;" `
do
 printf "%s : " $bdd
 psql -d $bdd -c "vacuum analyze ;"
done
echo "END TIME   - "$(date)
```
#### auto-vacuum

Il existe un process d'auto-vacuum. Celui-ci marque par exemple les enregistrements à supprimer. Selon la configuration il peut être assez consommateur en ressources. Il faut donc vérifier ces déclenchements et durée.

## Administration
### ajouter un utilisateur distant

dans le fichier `/var/lib/pgsql/9.5/data/pg_hba.conf`

> TODO: à affiner, ne pas utiliser all all !
{.is-warning}

```
host    all         all         X.X.X.X/32       md5
```

puis 

```
pg_ctl reload
```

### analyse de requête

Les requêtes lentes peuvent être *logguer* à partir de leur durée d'éxécution avec le paramètre `log_min_duration_statement = 500` (> 500ms dans l'exemple)  dans `postgresq.conf`.

#### schéma d'exécution

on peut préfixer la requête par `explain (ANALYZE, COSTS, VERBOSE, BUFFERS, FORMAT JSON) <requete_sql>`.

les données obtenues peuvent être *parsées* sur le site de Dalibo : https://explain.dalibo.com/

Le plan permet de vérifier s'il y a des parcours de tables séquentiels couteux ou l'utilisation d'index non performant.

### psql

dans `psql` on peut utiliser : 

```bash
# Pour lister les bases
\l
# Pour afficher la tailles des bases
\l+
# Pour se connecter à une base particulière
\c <nom_de_la_base>
# Pour changer le mode d'affichage en étendu
\x auto
# Pour sortir
\q
# Une fois connecté à une base
# pour décrire les colonnes d'une table
\d <nom_de_la_table>
# pour la description et la taille d'une table
\dt+ [<nom_de_la_table>]
```

### requêtes utiles

```sql
# pour afficher les requêtes en cours
select datname, usename, client_addr, query from pg_stat_activity;
# pour afficher les requêtes lockées
select datname, usename, client_addr, query from pg_stat_activity where waiting='t';

# afficher les informations sur une table
# on retrouve la dernière maj de stats ou de vacuum
# mais aussi les parcours sequentiel et usage d'index
# la colonne n_live_tup donne un nombre d'enregistrement approx
select * from pg_stat_user_tables where relname="<nom_de_la_table>";

```

Pour plus de détails sur lock en cours : 

```sql
SELECT
  nom_base,
  schema_objet_locke,
  nom_objet_locke,
  type_objet_locke,
  duree_bloquage,
  pid_session_bloquante,
  user_session_bloquante,
  client_session_bloquante,
  derniere_requete_session_bloquante,
  heure_debut_session_bloquante,
  heure_debut_requete_bloquante,
  pid_session_bloquee,
  user_session_bloquee,
  client_session_bloquee,
  derniere_requete_session_bloquee,
  heure_debut_requete_bloquee,
  heure_debut_session_bloquee
FROM 
  (
    SELECT distinct 
      RANK() OVER (PARTITION BY c.pid ORDER BY g.query_start DESC) as rang,
      c.datname AS nom_base, 
      e.nspname AS schema_objet_locke,
      d.relname AS nom_objet_locke,
      CASE 
        WHEN d.relkind IN ('t','r') THEN 'table' 
        WHEN d.relkind = 'i' THEN 'index' 
        WHEN d.relkind = 's' THEN 'sequence' 
        WHEN d.relkind = 'v' THEN 'vue' 
        ELSE d.relkind::text 
        END AS type_objet_locke,
      TO_CHAR(now()-c.query_start,'DD')||'j '||TO_CHAR(now()-c.query_start,'HH24:MI:SS') AS duree_bloquage,
      g.pid AS pid_session_bloquante,
      g.usename AS user_session_bloquante,
      g.client_addr AS client_session_bloquante, 
      g.query AS derniere_requete_session_bloquante, 
      TO_CHAR(g.backend_start,'YYYYMMDD HH24:MI:SS') AS heure_debut_session_bloquante,
      TO_CHAR(g.query_start,'YYYYMMDD HH24:MI:SS') AS heure_debut_requete_bloquante,
      c.pid AS pid_session_bloquee, 
      c.usename AS user_session_bloquee,
      c.client_addr AS client_session_bloquee, 
      c.query AS derniere_requete_session_bloquee, 
      TO_CHAR(c.query_start,'YYYYMMDD HH24:MI:SS') AS heure_debut_requete_bloquee,
      TO_CHAR(c.backend_start,'YYYYMMDD HH24:MI:SS') AS heure_debut_session_bloquee
      FROM 
        pg_locks AS a,
        pg_locks AS b,
        pg_stat_activity AS c,
        pg_class AS d,
        pg_namespace AS e,
        pg_locks AS f,
        pg_stat_activity AS g
      WHERE a.pid = b.pid
        AND a.pid = c.pid
        AND b.relation = d.oid
        AND d.relnamespace = e.oid
        AND b.relation = f.relation
        AND b.pid <> f.pid
        AND f.pid = g.pid
        AND c.query_start >= g.query_start
        AND a.granted IS FALSE
        AND b.relation::regclass IS NOT NULL
        AND e.nspname NOT IN ('pg_catalog','pg_toast','information_schema')
        AND e.nspname NOT LIKE 'pg_temp_%'
        AND f.granted is true
  ) AS resultat
WHERE rang = 1
ORDER BY resultat.heure_debut_requete_bloquee,resultat.heure_debut_requete_bloquante ;
```

### analyse d'activité

#### pgbadger

```
# génère un fichier out.html avec différents rapports
pgbadger </chemin/fichier/de/log>
```

