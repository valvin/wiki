---
title: TTS (text to speech) et STT (speech to text)
description: outils pour faire parler son ordinateur
published: true
date: 2022-07-03T08:52:02.405Z
tags: tts, voix
editor: markdown
dateCreated: 2020-12-23T11:23:07.590Z
---

> notes sur les recherches autour du *Text-To-Speech*
{.is-info}

## Text to speech (TTS)

Transformer un texte en signal audio

### espeak + mbrola

espeak s'installe depuis les dépôts.

mbrola est disponible sur github [ici](https://github.com/numediart/MBROLA) et les voix sont disponibles [ici](https://github.com/numediart/MBROLA-voices)

easyspeak intègre quelques voix mbrola mais pas toute. Voici une commande pour utiliser d'autres voix

```
espeak -v mb-fr1 -q -s 130 --pho -f text.txt| mbrola -f 1.1 -v 0.8  ~/work/tmp/MBROLA-voices/data/fr3/fr3 - -|play -r16000 -fS16
```

* `-s 130` : permet de lire 130 mots/minutes
* `-f 1.1` : augmente le *pitch* 
* `-v 0.8` : baisse le volume

### TTS et *machine learning*

#### Tacotron

projet d'entrainement de voix synthétique. Visiblement c'est le modèle utilisé par Google.

un modèle de Tacotron est [WaveRNN](https://github.com/fatchord/WaveRNN). Il est facile à tester mais sur le dépôt officiel il n'y a que l'anglais de disponible.

Il existe une version multi-langues sur [ce dépôt](https://github.com/Tomiinek/Multilingual_Text_to_Speech) dont on peut voir [le rapport](https://tomiinek.github.io/multilingual_speech_samples/). Tomiinek utilise Tacotron 2 avec le modèle WaveRNN. Il est possible de tester leur solution sur [Colab](https://colab.research.google.com/github/Tomiinek/Multilingual_Text_to_Speech/)


#### Coqui-AI TTS

Sur la base de tacotron c'est assez simple à tester:

```
python -m venv venv
. ./venv/bin/activate
pip install TTS
tts --list_models
tts --text "bonjour. comment t'appelles-tu?" --model_name "tts_models/fr/mai/tacotron2-DDC" --vocoder_name "vocoder_models/universal/libri-tts/wavegrad" --out_path speech.wav
```

Malheureusement, la phase vocoder du français ne semble pas bien supporté pour l'instant et la qualité de la voix n'est pas au rendez-vous avec le *vcoder* par défaut. Celui de la ligne de commande fonctionne mais consomme énormément de ressources. ([commentaire sur le sujet](https://github.com/coqui-ai/TTS/discussions/458))


#### Mimic 3

Venant du projet [MyCroft](https://mycroft.ai), Mimic 3 supporte beaucoup de langue dont le français et est compatible avec les apis MaryTTS.

pour le tester c'est très simple:

```
podman run --rm -it -p 59125:59125 -v "${HOME}/.local/share/mycroft/mimic3:/home/mimic3/.local/share/mycroft/mimic3:z" mycroftai/mimic3
```

puis sur l'interface web, sélectionner la langue et le texte à lire.

Plus d'info [ici](https://mycroft-ai.gitbook.io/docs/mycroft-technologies/mimic-tts/mimic-3#web-server)

## Speech to text (STT)

### vosk

en suivant ce tutoriel on peut facilement tester vosk : [ici](https://medium.com/analytics-vidhya/offline-speech-recognition-made-easy-with-vosk-c61f7b720215)

il faut installer quelques dépendances pour que cela soit opérationnels 

* pour fedora: `sudo dnf install portaudio-devel python3-pyaudio`
* les modèles souhaités en changeant le script pour utiliser la langue "fr"

### Mozilla voices

Mozilla voices collecte les voix à travers le monde et les fournit librement sous forme de dataset.

#### Deepspeech (plus maintenu)

Une des implémentations réalisée par mozilla est DeepSpeech : https://github.com/mozilla/DeepSpeech (dernière release 2020)

#### Autres

https://foundation.mozilla.org/en/blog/raise-your-voice-training-a-model-on-your-very-own-voice-clips-with-common-voice-and-coqui/

=> https://github.com/coqui-ai/STT
