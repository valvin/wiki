---
title: Git - configuration
description: configuration pour git
published: true
date: 2020-11-25T07:57:32+0100
tags: git, configuration, devops
---

En complément des [commandes git](/linux/commandes/git), voici un exemple de configuration pour gérer une utilisation multi-compte sur git. Mon cas d'usage est pro/perso.

Ma problématique était de pouvoir utiliser un nom et une adresse mail différente pour mes *commits*. Cela peut bien entendu s'étendre à tous les paramètres git.

Il est possible dans *git* d'inclure des configurations supplémentaires en fonction du répertoire dans lequel on se situe : [doc ici](https://git-scm.com/docs/git-config#_conditional_includes)

`~/.gitconfig`:

```
# This is Git's per-user configuration file.
[user]
# Please adapt and uncomment the following lines:
        name = <nom_principal>
        email = <email_principal>
[pull]
        ff = only
[core]
        editor = vim 
[includeIf "gitdir:~/work/pro/"]
    path = ~/.gitconfig_pro
```

`~/.gitconfig_pro`

```
[user]
        name = <nom_pro>
        email = <email pro>
```
