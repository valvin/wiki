---
title: Ansible - commandes
description: les commandes ansible que j'utilise
published: true
date: 2020-05-29T21:25:01.951Z
tags: commande, ansible
---

# Ansible

[Ansible](/devops/ansible) permet de déployer et coordonnées de nombreuses types d'actions.

Voici la liste des commandes que j'utilise régulièrement

> Les commandes ci-dessous spécifie l'inventaire mais il est préférable de le renseigner dans son fichier `ansible.cfg`.
{.is-info}
## Éxécution de *playbooks*

```bash
# éxécuter un playbook
ansible-playbook -i <chemin_inv> <fichier_playbook>
# éxécuter un playbook sur un groupe ou un hôte spécifique
ansible-playbook -i <chemin_inv> -l <groupe_ou_hôte> <fichier_playbook>
# éxécuter les tâches d'un playbook pour un tag spécifique
ansible-playbook -i <chemin_inv> -t <tag> <fichier_playbook>
# sasir le mot de passe vault à l'éxecution
ansible-playbook -i <chemin_inv> <fichier_playbook> --ask-vault-pass
# pour voir plein de logs et tenter de comprendre son problème
ansible-playbook -vvv -i <chemin_inv> <fichier_playbook>
```

## Modifier un fichier vault

```
ansible-vault edit <nom_du_fichier>
```

## Visualiser l'inventaire

```bash
# voir la totalité de l'inventaire et ses variables
ansible-inventory -i <chemin_inv> --list
# voir l'inventaire pour l'hôté spécifié
ansible-inventory -i <chemin_inv> --host <hôte>
```

## Fichier ansible.cfg

```ini
[defaults]
; nécessaires si les playbooks font appels à des rôles
roles_path = <chemin_répertoires_des_roles>
; permet de ne pas saisir l'inventaire à chaque commande
inventory = <chemin_fichier_inventaire>
; éviter de saisir le mot de passe vault (à utiliser avec précautions)
vault_password_file = <chemin_fichier_contenant_le_mot_de_passe_vault>
```