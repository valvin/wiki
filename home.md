---
title: Wiki de Valvin
description: Bienvenue chez moi! Je vais noter ici un peu tout et surtout rien ... bonne lecture!
published: true
date: 2022-11-20T18:16:07.058Z
tags: accueil
editor: markdown
dateCreated: 2020-04-08T20:03:37.317Z
---

# Bienvenue chez moi! 

Je souhaitais avoir un lieu où mettre des notes / infos dans un autre format que le blog. Le wiki semblait être adapté à ce besoin.

J'ai choisi [Wiki.js](/logiciels-libres/wikijs), jeune produit par rapport à un dokuwiki et autres moteurs de wiki mais je me suis laissé tenté.

Ce wiki à donc un usage avant tout personnel mais peut-être certainess de mes notes vous interreseront. L'idée c'est de "capitaliser" avec des notes sur des sujets "récurrents" comme par exemple:
* les logiciels [libres](/t/libre) que j'utilise
* l'usages de certaines [commandes](/t/commande) [linux](/t/linux) ou de requête pour les [bases de données](/t/bdd)
* des notes loisirs, comme sur l'[éléctronique](/t/electronique) ou le [jardinage](/t/jardinage).

Récemment, on m'a fait découvrir la notion de *jardin numérique* qui ressemble beaucoup à l'approche que j'ai de ce wiki. Voici un lien qui décrit l'idée derrière les [*digital garden*](https://maggieappleton.com/garden-history)

Le contenu est normalement indexé, n'hésitez pas à faire une petite recherche, la navigation par menu n'est pas "évidente".

Volontairement je souhaite mettre le contenu en français. Bien que plus universel, je trouve dommage que beaucoup d'information soit disponible uniquement en anglais.

## Contribution

S'agissant d'un wiki personnel, je ne souhaite pas créer d'utilisateurs. Cependant, si jamais vous voyez une coquille ou souhaitez apporter une précision, il est possible :

* d'ajouter un commentaire sur la page concernée
* de proposer une modifiction sur le dépôt git associé à ce wiki [ici](https://framagit.org/valvin/wiki).

## Licence

Sauf contre-indication spécifique, le contenu disponible ici est sous licence Creative-Commons [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).
Vous pouvez le partager, le modifier et même le vendre du moment où :

* l'attribution de l'auteur est signalée, par exemple : "valvin - https://wiki.valvin.fr"
* les modifications apportées sous distribuées sous la même licence
